// Everything in the %{ ... %} block is simply copied verbatim to the resulting wrapper file created by SWIG.
// This section is almost always used to include header files and other declarations that are required to make the generated wrapper code compile.
// It is important to emphasize that just because you include a declaration in a SWIG input file, that declaration does not automatically appear in the
// generated wrapper code---therefore you need to make sure you include the proper header files in the %{ ... %} section. It should be noted that the text enclosed
// in %{ ... %} is not parsed or interpreted by SWIG. The %{...%} syntax and semantics in SWIG is analogous to that of the declarations section used in input
// files to parser generation tools such as yacc or bison.

//--------------------------------------------------------------------------------------------------------------------------------
// here the code for the c/c++ side of the interface
//--------------------------------------------------------------------------------------------------------------------------------

%module NEFpp
%{

#define SWIG_FILE_WITH_INIT

#include <Eigen/Core>
#include <Eigen/SparseCore>

#include "nefpp_interface.hpp"

template<typename ptr>
ptr* delete_cpp_ptr(ptr* p)
{
  if(p != nullptr)
  {
    // std::cout << p << "\n";
    // std::cout << "Call to delete_cpp_ptr...\n";
    // delete (ptr*)p;
    delete p;
  }
  
  p = nullptr;
  // std::cout << p << "\n";
  return p;
}

%} // end of module{} section

//--------------------------------------------------------------------------------------------------------------------------------
// below the code required for generating the SWIG itf.
//--------------------------------------------------------------------------------------------------------------------------------

%include "stdint.i"
%include "std_except.i"
%include "std_string.i"
%include "std_vector.i"
%include "std_pair.i"
%include "std_map.i"
%include "std_unordered_map.i"

%include "config.hpp"

%include "data_structs.hpp"
%include "OS_specifics.hpp"
%include "json_parser.hpp"

%newobject blsurf_frac::read_vertices_file;
%newobject blsurf_frac::read_poly_BC_edges;
%newobject blsurf_frac::read_polygons_file;

%rename(read_vertices)  blsurf_frac::read_vertices_file;
%rename(read_edges)     blsurf_frac::read_poly_BC_edges;
%rename(read_polygons)  blsurf_frac::read_polygons_file;

%include "blsurf_loader.hpp"

%template(poly_BC_edges_list)   std::vector<blsurf_frac::poly_BC_edges>;

%template(pairFracIDVertexID)   std::pair<blsurf_frac::fracID,blsurf_frac::vertexID>;
%template(vertex_hash)          blsurf_frac::pair_hash<blsurf_frac::fracID,blsurf_frac::vertexID>;
%template(vertices_list)        std::unordered_map<blsurf_frac::vertex_key,blsurf_frac::vertex_data,blsurf_frac::vertex_hash>;

%template(pairFracIDPolygonID)  std::pair<blsurf_frac::fracID,blsurf_frac::polygonID>;
%template(polygon_hash)         blsurf_frac::pair_hash<blsurf_frac::fracID,blsurf_frac::polygonID>;
%template(polygons_list)        std::unordered_map<blsurf_frac::polygon_key,blsurf_frac::polygon_data,blsurf_frac::polygon_hash>;

%template(read_transmissivity)  blsurf_frac::read_transm_file<blsurf_frac::polygons_list>;

%include "methods/implementation_hho/utils_hho.hpp"

%include "diskpp_interface.hpp"

%include "nefpp_interface.hpp"

%template(vector_double) std::vector<double>;
%template(vector_int64)  std::vector<long long int>;

%template(pair_lli_lli) std::pair<long long int,long long int>;

%newobject assemble_system;

template<typename ptr>
ptr* delete_cpp_ptr(ptr* p);

%template(delete_diskpp_mesh) delete_cpp_ptr<mesh_type>;
%template(delete_diskpp_bc)   delete_cpp_ptr<bc_type>;

%template(delete_diskpp_asm)  delete_cpp_ptr<diskpp_itf::custom_assembler<mesh_type,long long int>>;

%template(assemble_system)  do_assembling<long long int>;

%template(solve_system) solve<long long int>;

%template(compute_properties) compute_nef_properties<long long int>;

%template(vector_of_sparse_triplet) std::vector<SparseMatrixTriplet>;

%template(pair_bcid_bcval) std::pair<long long int,double>;
%template(vector_pair_bcid_bcval) std::vector<std::pair<long long int,double>>;
