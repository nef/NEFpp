/**
 * @file solve_system.hpp
 * @author Florent Hédin, Géraldine Pichot (devel@fhedin.com, geraldine.pichot@inria.fr)
 * @brief This header only piece of code calls the required Eigen interfaces for solving the linear system.
 * @date 22/10/2020
 * 
 * @copyright INRIA Paris, EPI SERENA, 2020
 * 
 */
#pragma once

#include <cstdlib>

#include <tuple>
#include <iostream>
#include <memory>
#include <stdexcept>
#include <chrono>
#include <functional>

#include <Eigen/OrderingMethods>

#include <Eigen/SparseCore>
#include <Eigen/SparseLU>
#include <Eigen/SparseCholesky>

#include "config.hpp"

#ifdef HAVE_INTEL_MKL
#include <Eigen/PardisoSupport>
#endif

#include "data_structs.hpp"

#ifdef HAVE_INTEL_MKL

/**
 * @brief Class inheriting the VerboseEigenPardisoLU, this was the only way to set the verbosity level of the solver.
 * 
 * @tparam SparseMatrixType The Eigen sparse matrix type, we only use it to check what is the underlying integer type used for storing indices.
 */
template<typename SparseMatrixType>
class VerboseEigenPardisoLU : public Eigen::PardisoLU<SparseMatrixType>
{
  using StorageIndex = typename SparseMatrixType::StorageIndex;
  using Scalar = typename SparseMatrixType::Scalar;
  using Base = Eigen::PardisoLU<SparseMatrixType>;

public:

  /**
   * @brief Construct a new EigenPardisoLU solver object
   * 
   * @param wantVerboseSolver This bool will make the Intel PARDISO write statistics to the stdout when solving the system ; defaults to false
   */
  VerboseEigenPardisoLU(bool wantVerboseSolver = false)
  {
    this->m_msglvl = wantVerboseSolver ? 1 : 0;
    const std::string msg = (sizeof(StorageIndex) == sizeof(long long int)) ? "Using the 64-bit Pardiso interface." : "Using the 32-bit Pardiso interface.";
    std::cout << msg << std::endl;
  }

};

/**
 * @brief Class inheriting the VerboseEigenPardisoLLT, this was the only way to set the verbosity level of the solver.
 * 
 * @tparam SparseMatrixType The Eigen sparse matrix type, we only use it to check what is the underlying integer type used for storing indices.
 */
template<typename SparseMatrixType>
class VerboseEigenPardisoLLT : public Eigen::PardisoLLT<SparseMatrixType>
{
  using StorageIndex =  typename SparseMatrixType::StorageIndex;
  using Scalar = typename SparseMatrixType::Scalar;
  using Base = Eigen::PardisoLLT<SparseMatrixType>;

public:
  /**
   * @brief Construct a new EigenPardisoLLT solver object
   * 
   * @param wantVerboseSolver This bool will make the Intel PARDISO write statistics to the stdout when solving the system ; defaults to false
   */
  VerboseEigenPardisoLLT(bool wantVerboseSolver = false)
  {
    this->m_msglvl = wantVerboseSolver ? 1 : 0;
    const std::string msg = (sizeof(StorageIndex) == sizeof(long long int)) ? "Using the 64-bit Pardiso interface." : "Using the 32-bit Pardiso interface.";
    std::cout << msg << std::endl;
  }

};

/**
 * @brief Class inheriting the VerboseEigenPardisoLDLT, this was the only way to set the verbosity level of the solver.
 * 
 * @tparam SparseMatrixType The Eigen sparse matrix type, we only use it to check what is the underlying integer type used for storing indices.
 */
template<typename SparseMatrixType>
class VerboseEigenPardisoLDLT : public Eigen::PardisoLDLT<SparseMatrixType>
{
  using StorageIndex =  typename SparseMatrixType::StorageIndex;
  using Scalar = typename SparseMatrixType::Scalar;
  using Base = Eigen::PardisoLDLT<SparseMatrixType>;

public:
  /**
   * @brief Construct a new  EigenPardisoLDLT solver object
   * 
   * @param wantVerboseSolver This bool will make the Intel PARDISO write statistics to the stdout when solving the system ; defaults to false
   */
  VerboseEigenPardisoLDLT(bool wantVerboseSolver = false)
  {
    this->m_msglvl = wantVerboseSolver ? 1 : 0;
    const std::string msg = (sizeof(StorageIndex) == sizeof(long long int)) ? "Using the 64-bit Pardiso interface." : "Using the 32-bit Pardiso interface.";
    std::cout << msg << std::endl;
  }

};

#endif

/**
 * @brief This templated functions solves the linear system ; it will take care of calling the appropriate Eigen solver (internal solvers or Intel MKL PARDISO).
 * 
 * @tparam SpMat The Eigen type of the sparse matrix (left hand side - LHS).
 * @tparam vector_type The vector type of the right hand side (RHS).
 * @param S The sparse matrix (LHS).
 * @param SMB The second member (RHS).
 * @param solver_type The type of the solver parsed from the input JSON file.
 * @param minimum_IO Whether to reduce teminal output to the minimum or not ; defaults to false.
 * @param verbose_solver Whether to allow the solver to be verbose or not ; defaults to false.
 * @return Solution vector, the type is automatically deduced by the compiler based on types of S and SMB .
 */
template<typename SpMat, typename vector_type>
auto solve_system(const SpMat& S,
                  const vector_type& SMB,
                  Available_SparseSolvers solver_type = Available_SparseSolvers::EigenSimplicialLLT,
                  bool minimum_IO = false,
                  bool verbose_solver = false
)
{
  /*
   * This is the lambda function performing the solve step ; solver is a rvalue reference so that the lifefetime
   * of the Eigen solving interface is restricted.
   * 
   * This lambda accepts any type of Eigen sparse solver.
   * 
   */
  const auto compute_and_solve = [&](auto&& solver)
  {
    /*
     * The first step when solving a sparse system with Eigen is to perform the analyzePattern procedure : 
     * it computes the ordering permutation vector from the structural pattern of the matrix S.
     */
    auto start = std::chrono::high_resolution_clock::now();
    solver.analyzePattern(S);
    auto end = std::chrono::high_resolution_clock::now();
    auto elapsed = std::chrono::duration_cast<std::chrono::milliseconds>(end-start);

    if(solver.info() != Eigen::Success)
      throw std::runtime_error("Solver failure at step : 'solver.analyzePattern(S);' --> error code = " + std::to_string(solver.info()) + "\n");

    if(!minimum_IO)
      std::cout << "\tTime required for analyzing the matrix = " << elapsed.count() << " milliseconds ..." << std::endl;

    /*
     * The the numerical factorization is performed as step 2
     */
    start = std::chrono::high_resolution_clock::now();
    solver.factorize(S);
    end = std::chrono::high_resolution_clock::now();
    elapsed = std::chrono::duration_cast<std::chrono::milliseconds>(end-start);

    if(solver.info() != Eigen::Success)
      throw std::runtime_error("Solver failure at step : 'solver.factorize(S);' --> error code = " + std::to_string(solver.info()) + "\n");

    if(!minimum_IO)
      std::cout << "\tTime required for factorizing the matrix = " << elapsed.count() << " milliseconds ..." << std::endl;

    // then the solving procedure is called as a third and final step
    start = std::chrono::high_resolution_clock::now();
    const vector_type solution = solver.solve(SMB);
    end = std::chrono::high_resolution_clock::now();
    elapsed = std::chrono::duration_cast<std::chrono::milliseconds>(end-start);

    if(solver.info() != Eigen::Success)
      throw std::runtime_error("Solver failure at step : 'solver.solve(SMB);' --> error code = " + std::to_string(solver.info()) + "\n");

    if(!minimum_IO)
      std::cout << "\tTime required for solving the system = " << elapsed.count() << " milliseconds ..." << std::endl;

    return solution;
  };

  // Now : deduce integer index and scalar types, check the type of solver, and call the lambda with a Eigen solver interface created on the fly (rvalue reference trick)
  using StorageIndex = typename SpMat::StorageIndex;
  using Scalar = typename SpMat::Scalar;

  vector_type Tp;

  switch(solver_type)
  {

    // if MKL disabled we have only 2 choices : EigenSparseLU ...
    case Available_SparseSolvers::EigenSparseLU : 
      std::cout << "Solving using SparseLU..." << std::endl;
      Tp = compute_and_solve(Eigen::SparseLU<SpMat,Eigen::COLAMDOrdering<StorageIndex>>());
    break;

    // ... or EigenSimplicialLLT ; both will probably fails on medium to large sparse matrices !
    case Available_SparseSolvers::EigenSimplicialLLT : 
      std::cout << "Solving using SimplicialLLT..." << std::endl;
      Tp = compute_and_solve(Eigen::SimplicialLLT<SpMat,Eigen::Lower,Eigen::AMDOrdering<StorageIndex>>());
    break;

#ifdef HAVE_INTEL_MKL
    // therefore although Intel MKL use is optionnal, for studying meaningful systems you don't really have a choice !
    case Available_SparseSolvers::IntelPardisoLU :
    {
      std::cout << "Solving using PardisoLU..." << std::endl;

      // one can use preconditioning by defining the proper environment variable ; see the readme and the example python file for test cases.
      bool use_preconditioner = (std::getenv("NEFPP_PARDISO_PRECONDITIONER") != nullptr);
      
      VerboseEigenPardisoLU<SpMat> solver(verbose_solver);

      auto& params = solver.pardisoParameterArray();

      /*
       * Intel PARDISO solvers can benefit from multicore cpus during (some parts of) the analyzePattern, factorize and solve steps
       * Remember to define MKL_NUM_THREADS (similar use as OMP_NUM_THREADS) for manually setting the number of CPU cores.
       * Default is to use all available CPU cores.
       */
      #ifdef _OPENMP   
        // use of OpenMP for reordering the matrix
        params[1]  = 3; 
        // required when setting params[1] to 3
        params[33] = 0;
      #endif

      if(use_preconditioner)
      {
        params[3] = std::stoi(std::string(std::getenv("NEFPP_PARDISO_PRECONDITIONER")));
      }

      Tp = compute_and_solve(solver);

      if(use_preconditioner)
      {
        if(params[19]<0)
          throw std::runtime_error("Pardiso LU problem with the preconditioner, error code (iparm[19]) = " + std::to_string(params[19]) + "\n");
        else if(verbose_solver)
          std::cout << "Pardiso LU CGS diagnostics : number of completed iterations = " << params[19] << '\n'; 
      }

    }
    break;

    case Available_SparseSolvers::IntelPardisoLLT :
    {
      std::cout << "Solving using PardisoLLT..." << std::endl;

      bool use_preconditioner = (std::getenv("NEFPP_PARDISO_PRECONDITIONER") != nullptr);

      VerboseEigenPardisoLLT<SpMat> solver(verbose_solver);

      auto& params = solver.pardisoParameterArray();

      #ifdef _OPENMP   
        // use of OpenMP for reordering the matrix
        params[1]  = 3; 
        // required when setting params[1] to 3
        params[33] = 0;
      #endif

      if(use_preconditioner)
      {
        params[3] = std::stoi(std::string(std::getenv("NEFPP_PARDISO_PRECONDITIONER")));
      }

      Tp = compute_and_solve(solver);

      if(use_preconditioner)
      {
        if(params[19]<0)
          throw std::runtime_error("Pardiso LLT problem with the preconditioner, error code (iparm[19]) = " + std::to_string(params[19]) + "\n");
        else if(verbose_solver)
          std::cout << "Pardiso LLT CGS diagnostics : number of completed iterations = " << params[19] << '\n'; 
      }

    }
    break;

    case Available_SparseSolvers::IntelPardisoLDLT :
    {
      std::cout << "Solving using PardisoLDLT..." << std::endl;

      bool use_preconditioner = (std::getenv("NEFPP_PARDISO_PRECONDITIONER") != nullptr);

      VerboseEigenPardisoLDLT<SpMat> solver(verbose_solver);

      auto& params = solver.pardisoParameterArray();

      #ifdef _OPENMP   
        // use of OpenMP for reordering the matrix
        params[1]  = 3; 
        // required when setting params[1] to 3
        params[33] = 0;
      #endif

      if(use_preconditioner)
      {
        params[3] = std::stoi(std::string(std::getenv("NEFPP_PARDISO_PRECONDITIONER")));
      }

      Tp = compute_and_solve(solver);

      if(use_preconditioner)
      {
        if(params[19]<0)
          throw std::runtime_error("Pardiso LDLT problem with the preconditioner, error code (iparm[19]) = " + std::to_string(params[19]) + "\n");
        else if(verbose_solver)
          std::cout << "Pardiso LDLT CGS diagnostics : number of completed iterations = " << params[19] << '\n'; 
      }

    }
    break;

#endif

    default : 
      throw std::runtime_error("Unsupported solver type !");
    break;
  }

  // compute residual
  const Scalar rs = (S*Tp-SMB).norm()/SMB.norm();

  if(rs > Scalar(1.0e-10))
    throw std::runtime_error("Residual is "+ std::to_string(rs)  +" so larger than 1.0e-10, there was most probably something wrong with the solver !!!");

  if(std::isnan(rs))
    throw std::runtime_error("Residual is NaN, something wrong happened with the solver !!!");

  return std::make_pair(Tp,rs);
}

