#pragma once

#include <cstdint>

#include <string>

#include "config.hpp"

/**
 * @brief This lists the available sparse solversthis application can call.
 * 
 * To add a new solver : 1) Write the appropriate code in solve_system.hpp
 *                       2) Add it to this enum data structure, if possible guarding it with an appropriate #ifdef ... #endif
 *                       3) Modify the section from 'json_parser.cpp' which recognises the type of solver required by the user.
 */
enum Available_SparseSolvers
{
  EigenSparseLU,      ///< Eigen Sparse LU solver, see https://eigen.tuxfamily.org/dox/classEigen_1_1SparseLU.html
  EigenSimplicialLLT,  ///< Eigen Simplicial LLT solver, see https://eigen.tuxfamily.org/dox/classEigen_1_1SimplicialLLT.html
#ifdef HAVE_INTEL_MKL
  
  IntelPardisoLU,     ///< Intel Pardiso LU solver, see https://software.intel.com/en-us/mkl-developer-reference-fortran-intel-mkl-pardiso-parallel-direct-sparse-solver-interface
  IntelPardisoLLT,    ///< Intel Pardiso LLT solver, see https://software.intel.com/en-us/mkl-developer-reference-fortran-intel-mkl-pardiso-parallel-direct-sparse-solver-interface
  IntelPardisoLDLT,   ///< Intel Pardiso LDLT solver, see https://software.intel.com/en-us/mkl-developer-reference-fortran-intel-mkl-pardiso-parallel-direct-sparse-solver-interface
#endif
  Dummy ///< unused, but required to avoid a swig crash on parsing if HAVE_INTEL_MKL is undefined
};

/**
 * @brief Some input parameters for the program, parsed from the input JSON file
 * 
 */
struct input_param_t
{
  // -------------------------------------------------
  // mandatory parameters
  // -------------------------------------------------
  std::string blsurf_mesh_directory = "";

  double Lx;
  double Ly;
  double Lz;

  size_t polynomial_order_k;

  double h1;
  double h2;

  Available_SparseSolvers solver_type = Available_SparseSolvers::EigenSimplicialLLT;

  bool hdf5_output = false;

  // -------------------------------------------------
  // optional parameters below
  // -------------------------------------------------
  
  bool has_transm_file = false;
  std::string transm_file_name = "";
  std::string transm_fracIDs_mapping = "";

  // only used if has_transm_file is false
  double Txx;
  double Tyy;
  double Txy; 

  std::string hdf5_output_fname = "";

};
