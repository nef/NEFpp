/**
 * @file OS_specifics.hpp
 * @author Florent Hédin, Géraldine Pichot (devel@fhedin.com, geraldine.pichot@inria.fr)
 * @brief This is the header for a file containing code which is system dependent, e.g. using defines such as RUN_ON_LINUX or RUN_ON_WINDOWS
 * @date 22/10/2020
 * 
 * @copyright INRIA Paris, EPI SERENA, 2020
 * 
 */

#pragma once

#include <string>

#include "config.hpp"

/**
 * @brief Dump the Eigen version information to stdout
 */
void dump_eigen_version();

/**
 * @brief Dump the environment variables to stdout
 */
void dump_env();

/**
* @brief Returns a string describing the runtime OS.
* 
* @return On a POSIX compliant system the string contains the equivalent of running the command 'uname -a' ; on Windows returns a fair enough description of the OS
*/
std::string get_OS_name();

/**
* @brief Returns the current working directory (CWD)
* 
* @return Path to the CWD
*/
std::string get_current_dir();

/**
 * @brief Returns true if fname corresponds to the OS specific Null device (see https://en.wikipedia.org/wiki/Null_device).
 * This can be used for ignoring output attempts if the user specified the null device.
 * 
 * @param fname A file path to test.
 * 
 * @return Whether fname points to the null device or not.
 */
bool is_null_device(const std::string& fname);

/**
 * @brief Get the null device name for the current OS
 * 
 * @return The name of the null device on the runtime OS
 */
inline const std::string get_null_device_name()
{
  #ifdef RUN_ON_WINDOWS
  return std::string("nul");
  #else
  return std::string("/dev/null");
  #endif
}

/**
 * @brief Get the path separator ("/" or "\") for the current OS.
 * 
 * @return The path separator on the runtime OS
 */
inline const std::string get_path_separator()
{
  #ifdef RUN_ON_WINDOWS
  return std::string("\\");
  #else
  return std::string("/");
  #endif
}

/**
 * @brief Returns the peak (maximum so far) resident set size RSS (physical
 *        memory use) measured in kiloBytes, or zero if the value cannot be
 *        determined on this OS.
 * 
 * @return The RSS in kiloBytes
 */

double getPeakRSS_kiB();

/**
 * @brief Returns the peak (maximum so far) resident set size RSS (physical
 *        memory use) measured in MegaBytes, or zero if the value cannot be
 *        determined on this OS.
 * 
 * @return The RSS in MegaBytes
 */
double getPeakRSS_MiB();

/**
 * @brief Returns the peak (maximum so far) resident set size RSS (physical
 *        memory use) measured in GigaBytes, or zero if the value cannot be
 *        determined on this OS.
 * 
 * @return The RSS in GigaBytes
 */
double getPeakRSS_GiB();
