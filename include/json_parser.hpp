/**
 * @file json_parser.cpp
 * @author Florent Hédin, Géraldine Pichot (devel@fhedin.com, geraldine.pichot@inria.fr)
 * @brief This is the header for the file containing a function parsing the JSON input file provided by the user ; it uses the LIT licensed nlohmann::json header only library, provided in the "external" directory.
 *
 * @date 22/10/2020
 * 
 * @copyright INRIA Paris, EPI SERENA, 2020
 * 
 */

#pragma once

#include <string>

#include "config.hpp"

#include "data_structs_lite.hpp"

/**
 * @brief This function parses the JSON input file containing : the path to the mesh files, parameters of the simulation, the type of solver, data output parameters, etc.
 * 
 * @param fname Path to the input JSON file, existence of the file should have be verified before calling this function !
 * @param verbose Whether to be verbose while parsing the JSON file or not, defaults to false.
 * @param minimum_IO Whether to reduce the text output to a minimum while parsing or not, defaults to false.
 * 
 * @return A data structure of type input_param_t containing data parsed from the input JSON file. 
 */
input_param_t parse_json_file(const std::string& fname, bool verbose = false, bool minimum_IO = false);
