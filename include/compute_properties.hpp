#pragma once

#include <iostream>
#include <stdexcept>

// general config for nef++ and diskpp
#include "config.hpp"

#include "diskpp_interface.hpp"
#include "eigen_utils.hpp"
#include "methods/hho"
#include "methods/implementation_hho/scalar_stabilization.hpp"

#include "data_structs.hpp"

namespace compute_properties
{
  template<typename MeshType, typename CellType, typename DegreeInfoType>
  double get_pressure_at_cell_bar(const MeshType& msh,
                                        const CellType& cl,
                                        const DegreeInfoType& hdi,
                                        const eigen_Vec& full_sol,
                                        bool verbose = false)
  {
    using point_type = typename MeshType::point_type;

    const point_type bar = disk::barycenter(msh, cl);
    const auto cb = disk::make_scalar_monomial_basis(msh, cl, hdi.cell_degree());
    const eigen_Vec cphi = cb.eval_functions(bar);

    const eigen_Vec cell_sol = full_sol.head(cb.size());
    const double pression_at_cell_bar = disk::eval(cell_sol, cphi);

    return pression_at_cell_bar;
  }

  template<typename MeshType, typename CellType, typename DegreeInfoType>
  eigen_Vec get_Tp_at_edges_bar(const MeshType& msh,
                                const CellType& cell,
                                const DegreeInfoType& hdi,
                                const eigen_Vec& full_sol,
                                bool verbose = false)
  {
    using point_type = typename MeshType::point_type;

    using edge_type = typename MeshType::face_type;
    using edge_ID = typename edge_type::id_type;

    const auto& edgs_ids = cell.faces_ids();
    const size_t num_of_edges_for_cell = edgs_ids.size();
    const size_t num_hho_cell_unknows = disk::scalar_basis_size( hdi.cell_degree(), MeshType::dimension);

    // the begining of full sol contains cell unknowns, skip them as we focus on edges unknowns
    const eigen_Vec begin_edg_sol = full_sol.segment(num_hho_cell_unknows,full_sol.size()-num_hho_cell_unknows);

    eigen_Vec Tp_on_cell_edges = eigen_Vec::Zero(num_of_edges_for_cell);

    // iterate over edges of cell an retrieve the average Tp value per edge using diskpp internal basis functions.
    size_t e_idx(0);
    for(const auto eid : edgs_ids)
    {
      const edge_type e = msh.backend_storage()->edges[eid];

      const point_type bar = disk::barycenter(msh, e);
      const auto fb = disk::make_scalar_monomial_basis(msh, e, hdi.face_degree());
      const eigen_Vec fphi = fb.eval_functions(bar);

      const eigen_Vec edg_sol = begin_edg_sol.segment(e_idx*fb.size(),fb.size());
      Tp_on_cell_edges[e_idx] = disk::eval(edg_sol, fphi);
      e_idx++;
    }

    return Tp_on_cell_edges;
  }

  // template<typename MeshType, typename CellType, typename DegreeInfoType, typename BC_type, typename AssemblerType, typename FuncTypeA, typename FuncTypeB>
  // void get_flux_stab_dg(const MeshType& msh,
  //                       const CellType& cl,
  //                       const DegreeInfoType& hdi,
  //                       const BC_type& bc,
  //                       const AssemblerType& assembler,
  //                       const FuncTypeA& rhs_fun,
  //                       const FuncTypeB& k_fun,
  //                       const eigen_Vec& Tp_diskpp,
  //                       eigen_rowVec& flux_per_edge,
  //                       bool verbose = false
  //                      )
  // {
  //   using point_type = typename MeshType::point_type;

  //   using edge_type = typename MeshType::face_type;
  //   using edge_ID = typename edge_type::id_type;

  //   const auto cb = disk::make_scalar_monomial_basis(msh, cl, hdi.cell_degree());
  //   const std::pair<eigen_Mat, eigen_Mat> gr = disk::make_vector_hho_gradrec(msh, cl, hdi, k_fun);
  //   const eigen_Vec rhs = disk::make_rhs(msh, cl, cb, rhs_fun);

  //   const eigen_Mat stab = disk::make_scalar_dg_stabilization(msh, cl, hdi);
  //   const double K_max = eigen_utils::compute_max_eigenvalue(k_fun(point_type(0.,0.)));
  //   const double hT = disk::diameter(msh, cl);   // maximum distance between two different points of the element.
  //   const eigen_Mat A = gr.second + K_max * hT * stab; // rescaling of stab with max value of the K matrix

  //   const eigen_Vec locsol = assembler.take_local_data(msh, cl, bc, Tp_diskpp);

  //   const eigen_Vec fullsol = disk::make_scalar_static_decondensation(msh, cl, hdi, A, rhs, locsol);

  //   const eigen_Vec grad_u = gr.first * fullsol;
  //   const auto gb = disk::make_vector_monomial_basis(msh, cl, hdi.grad_degree());

  //   assert(grad_u.size() == gb.size());

  //   const auto cbs = cb.size();
  //   const eigen_Vec uT = fullsol.head(cbs);
  //   const auto fcs = disk::faces(msh, cl);

  //   size_t fc_off = 0;
  //   for(const edge_type& fc : fcs)
  //   {
  //     const auto fb = disk::make_scalar_monomial_basis(msh, fc, hdi.face_degree());
  //     const auto fbs = fb.size();

  //     const edge_ID eid = msh.lookup(fc);

  //     const auto hf = disk::diameter(msh, fc);
  //     const auto no = disk::normal(msh, cl, fc);
  //     const eigen_Vec uF = locsol.segment(fc_off, fbs);

  //     // over-intergation by security
  //     const auto qpf = disk::integrate(msh, fc, hdi.grad_degree() + 2);

  //     // compute fluxes F = grad(u).n + coeff_stab*(uF-uT)
  //     double flux_grad(0.);
  //     double flux_stab(0.);

  //     for (const auto& qp : qpf)
  //     {
  //       // eval velocity
  //       const auto gphi = gb.eval_functions(qp.point());
  //       const auto grad = disk::eval(grad_u, gphi);
  //       flux_grad += qp.weight() * (k_fun(qp.point()) * grad).dot(no);

  //       // eval stabilization term
  //       const auto fphi = fb.eval_functions(qp.point());
  //       const auto cphi = cb.eval_functions(qp.point());
  //       flux_stab += qp.weight() * (disk::eval(uF, fphi) - disk::eval(uT, cphi)) * (hT / hf);
  //     }

  //     // std::cout << "Cell " << cl << " edge " << fc << " : [flux_grad , flux_stab] --> " << flux_grad << " , " << flux_stab << '\n';

  //     const double flux_equi = flux_grad + K_max * flux_stab;
  //     flux_per_edge(eid) += flux_equi;

  //     fc_off += fbs;

  //   }

  // }

  template<typename MeshType, typename CellType, typename DegreeInfoType, typename BC_type, typename AssemblerType, typename FuncTypeA, typename FuncTypeB, typename StabSize>
  void get_flux_stab_hdg(const MeshType& msh,
                         const CellType& cl,
                         const DegreeInfoType& hdi,
                         const BC_type& bc,
                         const AssemblerType& assembler,
                         const FuncTypeA& rhs_fun,
                         const FuncTypeB& k_fun,
                         const eigen_Vec& Tp_diskpp,
                         eigen_rowVec& flux_per_edge,
						 const StabSize   stab_diam_F,
                         bool verbose = false
                        )
  {
    using point_type = typename MeshType::point_type;

    using edge_type = typename MeshType::face_type;
    using edge_ID = typename edge_type::id_type;

    const auto cb = disk::make_scalar_monomial_basis(msh, cl, hdi.cell_degree());
    const std::pair<eigen_Mat, eigen_Mat> gr = disk::make_vector_hho_gradrec(msh, cl, hdi, k_fun);
    const eigen_Vec rhs = disk::make_rhs(msh, cl, cb, rhs_fun);
	//StabSize   stab_diam_F   = StabSize::hF;

    const eigen_Mat stab = disk::make_scalar_hdg_stabilization(msh, cl, hdi,stab_diam_F);
    const double K_max = eigen_utils::compute_max_eigenvalue(k_fun(point_type(0.,0.)));

    const eigen_Mat A = gr.second + K_max * stab; // rescaling of stab with max value of the K matrix

    const eigen_Vec locsol = assembler.take_local_data(msh, cl, bc, Tp_diskpp);

    const eigen_Vec fullsol = disk::make_scalar_static_decondensation(msh, cl, hdi, A, rhs, locsol);

    // to compute equilibrated fluxes
    // if(verbose)
    // {
    //   std::cout << "gr.first size : [" << gr.first.rows() << "x" << gr.first.cols() << "]\n";
    //   std::cout << "fullsol size : [" << fullsol.rows() << "x" << fullsol.cols() << "]\n";
    //   std::cout.flush();

    //   std::cout.precision(std::numeric_limits<double>::digits10);

    //   DUMP(gr.first);
    //   DUMP(fullsol);
    //   std::cout.flush();

    //   std::cout << "gr.first.data() : " << gr.first.data() << " " << &(gr.first(0,0)) << "\n";
    //   std::cout << "fullsol.data() : " << fullsol.data() << " " << &(fullsol(0,0)) << "\n";
    //   std::cout.flush();
    // }
    const eigen_Vec grad_u = gr.first * fullsol;

    const auto gb = disk::make_vector_monomial_basis(msh, cl, hdi.grad_degree());
    assert(grad_u.size() == gb.size());

    const auto flux = disk::make_scalar_hdg_stabilization_adjoint(msh, cl, hdi, stab_diam_F); // DISKPP OLD WAS OK 
    // const eigen_Mat flux = disk::make_scalar_hdg_stabilization_diff(msh, cl, hdi);  // DISKPP BUG MATRIX SIZE 24

    // if(verbose)
    // {
    //   std::cout << "flux size : [" << flux.rows() << "x" << flux.cols() << "]\n";
    //   std::cout << "fullsol size : [" << fullsol.rows() << "x" << fullsol.cols() << "]\n";
    //   std::cout.flush();

    //   std::cout.precision(std::numeric_limits<double>::digits10);

    //   DUMP(flux);
    //   DUMP(fullsol);
    //   std::cout.flush();

    //   std::cout << "flux.data() : " << flux.data() << " " << &(flux(0,0)) << "\n";
    //   std::cout << "fullsol.data() : " << fullsol.data() << " " << &(fullsol(0,0)) << "\n";
    //   std::cout.flush();
    // }

    const eigen_Vec flux_u = flux * fullsol;

    // const auto cbs = cb.size();
    // const eigen_Vec uT = fullsol.head(cbs);

    const auto fcs = disk::faces(msh, cl);

    size_t fc_off = 0;
    for(const edge_type& fc : fcs)
    {
      const edge_ID eid = msh.lookup(fc);

      const auto diff_deg = std::max(hdi.face_degree(), hdi.cell_degree());

      const auto db = disk::make_scalar_monomial_basis(msh, fc, diff_deg);
      const auto dbs = db.size();

      const eigen_Vec flux_uF = flux_u.segment(fc_off, dbs);

      const auto no = disk::normal(msh, cl, fc);
      // over-intergation by security
      const auto qpf = disk::integrate(msh, fc, hdi.grad_degree() + 2);
      // const auto qpf = disk::integrate(msh, fc,20);

     // compute fluxes F = grad(u).n + coeff_stab*(uF-uT)
      double flux_grad(0.);
      double flux_stab(0.);

      for (const auto& qp : qpf)
      {
        // eval velocity
        const Eigen::Matrix<double, Eigen::Dynamic, 2> gphi = gb.eval_functions(qp.point());
        const Eigen::Matrix<double, 2, 1> grad = disk::eval<double,2>(grad_u, gphi);
        flux_grad += qp.weight() * (k_fun(qp.point()) * grad).dot(no);

        // eval stabilization term
        const auto dphi = db.eval_functions(qp.point());
        flux_stab += qp.weight() * disk::eval(flux_uF, dphi);
      } // loop on quadrature points

      // std::cout << "Cell " << cl << " edge " << fc << " : [flux_grad , flux_stab] --> " << flux_grad << " , " << flux_stab << '\n';

      const double flux_equi = flux_grad + K_max * flux_stab;
      flux_per_edge(eid) += flux_equi;

      fc_off += dbs;

    } // loop on faces

  } // end of function

  // template<typename MeshType, typename CellType, typename DegreeInfoType, typename BC_type, typename AssemblerType, typename FuncTypeA, typename FuncTypeB>
  // void get_flux_stab_laplacian(const MeshType& msh,
  //                              const CellType& cl,
  //                              const DegreeInfoType& hdi,
  //                              const BC_type& bc,
  //                              const AssemblerType& assembler,
  //                              const FuncTypeA& rhs_fun,
  //                              const FuncTypeB& k_fun,
  //                              const eigen_Vec& Tp_diskpp,
  //                              eigen_rowVec& flux_per_edge,
  //                              bool verbose = false
  //                              )
  // {
  //   using point_type = typename MeshType::point_type;

  //   using edge_type = typename MeshType::face_type;
  //   using edge_ID = typename edge_type::id_type;

  //   const double K_max = eigen_utils::compute_max_eigenvalue(k_fun(point_type(0.,0.)));

  //   const auto cb = disk::make_scalar_monomial_basis(msh, cl, hdi.cell_degree());
  //   const auto gr = disk::make_vector_hho_gradrec(msh, cl, hdi);
  //   const auto recons = disk::make_scalar_hho_laplacian(msh, cl, hdi).first;
  //   const auto stab = disk::make_scalar_hho_stabilization(msh, cl, recons, hdi);
  //   const auto rhs = disk::make_rhs(msh, cl, cb, rhs_fun, 2); // the 2 instead of the default of 0 makes the integration on more points 

  //   const eigen_Mat A = gr.second + K_max * stab;

  //   const eigen_Vec locsol = assembler.take_local_data(msh, cl, bc, Tp_diskpp);

  //   const eigen_Vec fullsol = disk::make_scalar_static_decondensation(msh, cl, hdi, A, rhs, locsol);

  //   // to compute equilbrated fluxes
  //   const eigen_Vec grad_u = gr.first * fullsol;
  //   const auto gb = disk::make_vector_monomial_basis(msh, cl, hdi.grad_degree());
  //   assert(grad_u.size() == gb.size());

  //   const auto flux = disk::make_scalar_hho_stabilization_adjoint(msh, cl, recons, hdi); // DISKPP CHANGE HERE
  //   // const auto flux = disk::make_scalar_hho_stabilization_fluxes(msh, cl, recons, hdi); // DISKPP CHANGE HERE
  //   const eigen_Vec flux_u = flux * fullsol;

  //   const auto fcs = disk::faces(msh, cl);

  //   size_t fc_off = 0;

  //   for (const edge_type& fc : fcs)
  //   {
  //     const edge_ID eid = msh.lookup(fc);

  //     const auto diff_deg = std::max(hdi.face_degree(), hdi.cell_degree());

  //     const auto db = disk::make_scalar_monomial_basis(msh, fc, diff_deg);
  //     const auto dbs = db.size();

  //     const eigen_Vec flux_uF = flux_u.segment(fc_off, dbs);

  //     const auto no = disk::normal(msh, cl, fc);
  //     // over-intergation by security
  //     const auto qpf = disk::integrate(msh, fc, hdi.grad_degree() + 2);

  //     // compute fluxes F = grad(u).n + coeff_stab*(uF-uT)
  //     double flux_grad(0.);
  //     double flux_stab(0.);

  //     for (auto& qp : qpf)
  //     {
  //       // eval velocity
  //       const auto gphi = gb.eval_functions(qp.point());
  //       const auto grad = disk::eval(grad_u, gphi);
  //       flux_grad += qp.weight() * (k_fun(qp.point()) * grad).dot(no);

  //       // eval stabilization term
  //       const auto dphi = db.eval_functions(qp.point());
  //       flux_stab += qp.weight() * (disk::eval(flux_uF, dphi));
  //     } // loop on quadrature points

  //     const double flux_equi = flux_grad + K_max * flux_stab;

  //     flux_per_edge(eid) += flux_equi;

  //     fc_off += dbs;
  //   } // loop on faces
 
  // } // end of function

  // template<typename MeshType, typename CellType, typename DegreeInfoType, typename BC_type, typename AssemblerType, typename FuncTypeA, typename FuncTypeB>
  // void get_flux_old_noStab_RT(const MeshType& msh,
  //                             const CellType& cl,
  //                             const DegreeInfoType& hdi,
  //                             const BC_type& bc,
  //                             const AssemblerType& assembler,
  //                             const FuncTypeA& rhs_fun,
  //                             const FuncTypeB& k_fun,
  //                             const eigen_Vec& Tp_diskpp,
  //                             eigen_rowVec& flux_per_edge,
  //                             bool verbose = false
  //                           )
  // {
  //   using point_type = typename MeshType::point_type;
  //   using edge_type = typename MeshType::face_type;
  //   using edge_ID = typename edge_type::id_type;

  //   const eigen_Vec loc_data = assembler.take_local_data(msh,cl,bc,Tp_diskpp);
  //   const auto cb  = disk::make_scalar_monomial_basis(msh, cl, hdi.cell_degree());
  //   const auto gr  = disk::make_vector_hho_gradrec_RT(msh, cl, hdi, k_fun);
  //   const auto rhs = disk::make_rhs(msh, cl, cb, rhs_fun);
  //   const auto full_sol = disk::make_scalar_static_decondensation(msh, cl, hdi, gr.second, rhs, loc_data);

  //   const eigen_Vec grad_u = gr.first * full_sol;
  //   const auto gb = disk::make_vector_monomial_basis_RT(msh, cl, hdi.grad_degree());
  //   const auto fcs = disk::faces(msh, cl);

  //   for(const auto& fc :fcs)
  //   {
  //     const auto bar = disk::barycenter(msh, fc);
  //     const auto no = disk::normal(msh, cl, fc);

  //     const auto qpf = disk::integrate(msh, fc, hdi.grad_degree());

  //     const edge_ID eid = msh.lookup(fc);

  //     double flux(0.0);

  //     for(const auto& qp : qpf)
  //     {
  //       // eval velocity
  //       const auto gphi = gb.eval_functions(qp.point());
  //       const auto grad = disk::eval(grad_u, gphi);
  //       flux += qp.weight() * (k_fun(qp.point())*grad).dot(no);
  //     }

  //     flux_per_edge(eid) += flux;

  //   }
  // }

  template<typename bcType>
  inline void accumulate_Q1_Q2_contributions(const bcType& bc,
                                             const disk::ident_raw_t& eid, const double& flux,
                                             double& Q1, double& Q2,
                                             bool verbose = false)
  {
    if(bc.is_dirichlet_face(eid))
    {
      const auto dirichlet_type = bc.dirichlet_boundary_id(eid);

      switch(dirichlet_type)
      {
        case diskpp_itf::BOUNDARY_FUNC_TYPE::DIRICHLET_H1 :
          Q1 += flux;
        break;

        case diskpp_itf::BOUNDARY_FUNC_TYPE::DIRICHLET_H2 :
          Q2 += flux;
        break;
      
        default:
          throw std::runtime_error("The impossible happened : unknown Dirichlet Boundary type !!");
        break;
      }
    }
  } // end accumulate_Q1_Q2_contributions

  template<typename bcType, typename edgeType>
  inline void accumulate_Q1_Q2_contributions(const bcType& bc,
                                             const edgeType& fc, const double& flux,
                                             double& Q1, double& Q2,
                                             bool verbose = false)
  {
    if(bc.is_dirichlet_face(fc))
    {
      const auto dirichlet_type = bc.dirichlet_boundary_id(fc);

      switch(dirichlet_type)
      {
        case diskpp_itf::BOUNDARY_FUNC_TYPE::DIRICHLET_H1 :
          Q1 += flux;
        break;

        case diskpp_itf::BOUNDARY_FUNC_TYPE::DIRICHLET_H2 :
          Q2 += flux;
        break;
      
        default:
          throw std::runtime_error("The impossible happened : unknown Dirichlet Boundary type !!");
        break;
      }
    }
  } // end accumulate_Q1_Q2_contributions

  template<typename meshType, typename bcType>
  inline void calculate_Q1_Q2_contributions(const meshType& msh, const bcType& bc,
                                             const eigen_rowVec& flux_per_edge,
                                             double& Q1, double& Q2,
                                             bool verbose = false)
  {
    using cell_type  = typename meshType::cell_type;
    using edge_type  = typename meshType::face_type;
    using point_type = typename meshType::point_type;

    using edge_id = typename edge_type::id_type;

    for(const cell_type& cell : msh)
    {
      for(const edge_type& fc : disk::faces(msh,cell))
      {
        const edge_id eid = msh.lookup(fc);

        if(bc.is_dirichlet_face(eid))
        {
          const auto dirichlet_type = bc.dirichlet_boundary_id(eid);

          switch(dirichlet_type)
          {
            case diskpp_itf::BOUNDARY_FUNC_TYPE::DIRICHLET_H1 :
              Q1 += flux_per_edge(eid);
            break;

            case diskpp_itf::BOUNDARY_FUNC_TYPE::DIRICHLET_H2 :
              Q2 += flux_per_edge(eid);
            break;
          
            default:
              throw std::runtime_error("The impossible happened : unknown Dirichlet Boundary type !!");
            break;
          } // end switch
        } // end if
      } // loop on edges
    } // loop on cells
  } // end accumulate_Q1_Q2_contributions

  inline double compute_equivalent_permeability(const input_param_t& inp, const double& Q1, const double& Q2, bool verbose = false)
  {
    const double delta_h = inp.h1 - inp.h2;
    const double eq_perm = -Q1*inp.Lx/(inp.Ly*inp.Lz*delta_h);
    return eq_perm;
  } // end compute_equivalent_permeability

} // end namespace compute_properties

