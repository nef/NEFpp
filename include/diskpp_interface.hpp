#pragma once

#include <utility>
#include <tuple>
#include <vector>
#include <iostream>
#include <stdexcept>
#include <algorithm>
#include <functional>
#include <limits>
#include <memory>

#include "config.hpp"
#include "blsurf_loader.hpp"

// diskpp headers
#include "geometry/geometry.hpp"
#include "boundary_conditions/boundary_conditions.hpp"
#include "methods/implementation_hho/scalar_stabilization.hpp"
#include "methods/hho"
// end diskpp headers

/**
 * @brief A namespace containing classes for interacting with Disk++
 * 
 */
namespace diskpp_itf
{

  /**
  * @brief An enumeration to represent the different IDs of the boundary edges we will define
  */
  enum BOUNDARY_FUNC_TYPE : size_t
  {
    NEUMANN_ZERO = 0,
    DIRICHLET_H1 = 1,
    DIRICHLET_H2 = 2,
    NONE = 42 
  };

  /**
   * @brief This will create a valid diskpp mesh from blsurf_frac data parsed from *.mesh files.
   * 
   * Cells are plain triangles.
   * 
   * @todo Document this
   * 
   * @tparam mesh_type 
   */
  template<typename mesh_type>
  class custom_triangular_mesh_loader_from_blsurf_files
  {
  public:

    using point_type   = typename mesh_type::point_type;
    using node_type    = typename mesh_type::node_type;
    using edge_type    = typename mesh_type::edge_type;
    using surface_type = typename mesh_type::surface_type;

    /**
    * @brief No need to build a custom ctor for now
    */
    custom_triangular_mesh_loader_from_blsurf_files() = default;
    
    /**
    * @brief No need to build a custom dtor for now
    */
    ~custom_triangular_mesh_loader_from_blsurf_files() = default;

    bool    verbose(void) const     { return m_verbose; }
    void    verbose(bool v)         { m_verbose = v; }
    
    bool custom_read(const blsurf_frac::vertices_list& bl_vertices,
                     const blsurf_frac::edges_list& bl_edges,
                     const blsurf_frac::triangles_list& bl_triangles)
    {
      
      if(this->verbose())
      {
        std::cout << " *** Creating a mesh from blsurf_frac *.mesh file ...  ***\n";
      }
      
      // count how many unique vertices there are by getting the max value of globalID 
      std::vector<blsurf_frac::vertexID> global_ids;
      for(const auto& blv : bl_vertices)
      {
        const auto& vdata = blv.second;
        global_ids.push_back(vdata.globalID);
      }
      const blsurf_frac::vertexID& max_globID = *(std::max_element(global_ids.begin(),global_ids.end()));

      // first read the input vector of points ; note how a unique id is generated using disk::point_identifier
      points.reserve(max_globID);
      nodes.reserve(max_globID);
      // we do store dummy coordinates for the moment because we will override them later when iterating over fractures in order to retrieve 2d coordinates
      for(blsurf_frac::vertexID i=0; i<max_globID; i++)
      {
        const point_type pt({0.0,0.0});
        points.push_back(pt);
        
        // see point.hpp and ident.hpp ; basically disk::point_identifier is used to be sure that there will never be 2 nodes with the same id
        const auto point_id = disk::point_identifier<2>(i);
        
        const auto node = node_type({point_id});
        nodes.push_back(node);
        
        if(this->verbose())
        {
          std::cout << "Read point " << pt << "' will be registered as node " << node << '\n';
        }
      }
      
      // read the list of triangles and also generate the list of edges
      for(const auto& blt : bl_triangles)
      {
        const auto& tr_key = blt.first;
        const auto& fractureID = tr_key.first;
        const auto& local_trID = tr_key.second;

        const auto& tr_data = blt.second;

        // for each triangle, use the 3 local vertices numbering in ordrer to retrieve the 3 global vertices numbering from bl_vertices
        using vkey = blsurf_frac::vertex_key;
        using vID  = blsurf_frac::vertexID;
        // bl_vertices.find returns an iterator to an element of the map : we keep 'second' which is a reference to a 'vertex_data' structure, and find global_ID from it ; 
        // there is also a -1 because the map stores globalID s for matlab, starting at 1.
        const vID vg0 = bl_vertices.find(vkey(fractureID,tr_data.vloc1))->second.globalID - (vID) 1u; 
        const vID vg1 = bl_vertices.find(vkey(fractureID,tr_data.vloc2))->second.globalID - (vID) 1u;
        const vID vg2 = bl_vertices.find(vkey(fractureID,tr_data.vloc3))->second.globalID - (vID) 1u;

        // generate diskpp identifiers from those
        const auto p0 = disk::point_identifier<2>(vg0);
        const auto p1 = disk::point_identifier<2>(vg1);
        const auto p2 = disk::point_identifier<2>(vg2);

        // create 3 edges on the fly from the identifiers ; blsurf_frac edges def is not used !
        const auto e0 = edge_type({p0,p1});
        const auto e1 = edge_type({p1,p2});
        const auto e2 = edge_type({p0,p2});
        
        edges.push_back(e0);
        edges.push_back(e1);
        edges.push_back(e2);
        
        const auto tri = surface_type({p0,p1,p2});
        
        surfaces.push_back(tri);
        
        if(this->verbose())
        {
          std::cout << "Triangle processed as " << tri << '\n';
          std::cout << "Three edges added from the triangle : " << e0 << " | " << e1 << " | "<< e2 << '\n';
        }
      }
      
      for(const auto& e : bl_edges)
      {
        const blsurf_frac::edge_key&  e_key   = e.first;
        const blsurf_frac::fracID& fractureID = e_key.first;
        const blsurf_frac::edgeID& e_localID  = e_key.second;

        const blsurf_frac::edge_data& e_data = e.second;

        bool is_dirichlet = false;
        diskpp_itf::BOUNDARY_FUNC_TYPE dpp_btype = diskpp_itf::BOUNDARY_FUNC_TYPE::NONE;

        /*
        * Now detect the type of boundary edges based on the content of data structure bl_bc_mapping (it is a map of edgeID--bc_data, for each edge of the system) ;
        * The following rules are used :
        * 
        *  + If bcd.lim_face is true then the edge is on a face of the cubic domain so either Dirichlet or Neumann (see bext step).
        *  + If bcd.lim_face was true, then bcd.lim_no_face contains the face number of the cube in which the edge resides (1 to 6), usually 1 and 2 are imposing 
        *       Dirichlet, the others Neumann. (TODO need to read this from a file at some point).
        *  + If bcd.lim_arc is true the edge is Neumann ; this can be the case for an edge not on a cube face.
        *  + If bcd.lim_inter is true the edge is at an intersection, and by convention it can't be Dirichlet AND intersection : therefore if true and
        *       if the edge was already marked Dirichlet, unmark it as Dirichlet.
        */
        if(e_data.lim_arc)
        {
          // always Neuman if lim_arc is true
          dpp_btype = diskpp_itf::BOUNDARY_FUNC_TYPE::NEUMANN_ZERO;
        }
        else if(e_data.lim_face)
        {
          // TODO do not hardcode H1 or H2 below
          // Dirichlet or Neumann depending on lim_no_face
          if(e_data.lim_no_face == 1)
          {
            dpp_btype = diskpp_itf::BOUNDARY_FUNC_TYPE::DIRICHLET_H1;
            is_dirichlet = true;
          }
          else if(e_data.lim_no_face == 2) 
          {
            dpp_btype = diskpp_itf::BOUNDARY_FUNC_TYPE::DIRICHLET_H2;
            is_dirichlet = true;
          }
          else
            dpp_btype = diskpp_itf::BOUNDARY_FUNC_TYPE::NEUMANN_ZERO;
        }
        
        if(e_data.lim_inter and is_dirichlet)
        {
          is_dirichlet = false;
          dpp_btype = diskpp_itf::BOUNDARY_FUNC_TYPE::NONE;
        }
        
        if(dpp_btype == diskpp_itf::BOUNDARY_FUNC_TYPE::NONE)
          continue;
        // else : need to be procesed as diskpp BC

        // first retrieve vertices global IDs coresponding to the 2 local ids stored within e_data
        const blsurf_frac::vertex_key key1(fractureID,e_data.vloc1);
        const blsurf_frac::vertex_key key2(fractureID,e_data.vloc2);

        // bl_vertices.find returns an iterator to an element of the map : we keep 'second' which is a reference to a 'vertex_data' structure, and find global_ID from it ; 
        // there is also a -1 because the map stores globalID s for matlab, starting at 1.
        const blsurf_frac::vertexID vg0 = bl_vertices.find(key1)->second.globalID - (blsurf_frac::vertexID) 1u;
        const blsurf_frac::vertexID vg1 = bl_vertices.find(key2)->second.globalID - (blsurf_frac::vertexID) 1u;

        // now build the diskpp data struct from this
        const auto p0 = disk::point_identifier<2>(vg0);
        const auto p1 = disk::point_identifier<2>(vg1);
        const auto bc_edge = edge_type({p0,p1});

        boundary_edges.push_back(std::make_pair(bc_edge,dpp_btype));
          
        if(this->verbose())
        {
          std::cout << "Read boundary edge " << bc_edge << " of ID " << dpp_btype << '\n';
        }

      } // for triangles - edges
      
      return true;
    } // method custom read
    
    bool populate_mesh(mesh_type& msh)
    {
      auto storage = msh.backend_storage();
      
      if(this->verbose())
      {
        std::cout << "Sorting and moving data to mesh's backend storage ..." << '\n';
      }

      storage->points = std::move(points);
      storage->nodes = std::move(nodes);

      points.clear();
      points.resize(0);
      nodes.clear();
      nodes.resize(0);

      /* sort edges, make unique and move them in geometry */
      disk::priv::sort_uniq(edges);
      storage->edges = std::move(edges);
      edges.clear();
      edges.resize(0);

      /* sort triangles, make unique and move them in geometry */
      disk::priv::sort_uniq(surfaces);
      storage->surfaces = std::move(surfaces);
      surfaces.clear();
      surfaces.resize(0);
 

      storage->boundary_info.resize(storage->edges.size());
      
      for (auto& be : boundary_edges)
      {
        const auto position = disk::find_element_id(storage->edges.cbegin(), storage->edges.cend(), be.first);
        
        assert(position.first);
        
        const disk::bnd_info bi({be.second, true});
        storage->boundary_info.at(position.second) = bi;
      }
      boundary_edges.clear();
      boundary_edges.resize(0);
 
      if(this->verbose())
      {

        std::cout << "Done !!" << '\n';
        
        std::cout << "Nodes: " << storage->nodes.size() << '\n';
        std::cout << "Edges: " << storage->edges.size() << '\n';
        std::cout << "Surfaces (triangles): " << storage->surfaces.size() << '\n';
        
      }
      
      return true;
    }
  private:
    
    std::vector<point_type>    points;
    std::vector<node_type>     nodes;
    std::vector<edge_type>     edges;
    std::vector<surface_type>  surfaces;
    
    std::vector<std::pair<edge_type,disk::ident_raw_t>>    boundary_edges;

    bool m_verbose;
    
  }; // end of class custom_triangular_mesh_loader_from_blsurf_files

  /**
   * @brief This will create a valid diskpp mesh from blsurf_frac data parsed from *.mesh files.
   * 
   * Cells are polygons with an arbitrary number of edges/vertices.
   * 
   * @todo Document this
   * 
   * @tparam mesh_type 
   */
  template<typename mesh_type>
  class custom_polygonal_mesh_loader_from_blsurf_files
  {
  public:

    using point_type   = typename mesh_type::point_type;
    using node_type    = typename mesh_type::node_type;
    using edge_type    = typename mesh_type::edge_type;
    using surface_type = typename mesh_type::surface_type;

    using node_ID = disk::identifier<node_type,disk::ident_raw_t,0>;

    /**
    * @brief No need to build a custom ctor for now
    */
    custom_polygonal_mesh_loader_from_blsurf_files() = default;
    
    /**
    * @brief No need to build a custom dtor for now
    */
    ~custom_polygonal_mesh_loader_from_blsurf_files() = default;

    bool    verbose(void) const     { return m_verbose; }
    void    verbose(bool v)         { m_verbose = v; }

    bool custom_read(const blsurf_frac::vertices_list& bl_vertices,
                    //  const blsurf_frac::edges_list& bl_edges,
                     const blsurf_frac::poly_BC_edges_list& bl_BC_edgs,
                     const blsurf_frac::polygons_list& bl_polygons)
    {

      if(this->verbose())
      {
        std::cout << " *** Creating a polygonal mesh from blsurf_frac *.mesh file ...  ***" << '\n';
      }
      
      // count how many unique vertices there are by getting the max value of globalID 
      std::vector<blsurf_frac::vertexID> global_ids;
      global_ids.reserve(bl_vertices.size());
      for(const auto& blv : bl_vertices)
      {
        const auto& vdata = blv.second;
        global_ids.push_back(vdata.globalID);
      }
      const blsurf_frac::vertexID& max_globID = *(std::max_element(global_ids.begin(),global_ids.end()));

      // first read the input vector of points ; note how a unique id is generated using disk::point_identifier
      points.reserve(max_globID);
      nodes.reserve(max_globID);
      // we do store dummy coordinates for the moment because we will override them later when iterating over fractures in order to retrieve 2d coordinates
      for(blsurf_frac::vertexID i=0; i<max_globID; i++)
      {
        const point_type pt({0.0,0.0});
        points.push_back(pt);
      
        const auto node = node_type(disk::point_identifier<2>(i));
        nodes.push_back(node);
        
        if(this->verbose())
        {
          std::cout << "Read point " << pt << "' will be registered as node " << node << '\n';
        }
      }
      
      polygons.reserve(bl_polygons.size());
      edges.reserve(bl_polygons.size()*3);
      // polygon data structure used to store a blsurf triangle
      // edges also generated in situ at this time
      for(const auto& [pl_key,pl_data] : bl_polygons)
      {
        const blsurf_frac::fracID&    fractureID = pl_key.first;
        const blsurf_frac::polygonID& local_plID = pl_key.second;

        const size_t num_vertices = pl_data.vertices.size();

        if(this->verbose())
          std::cout << "Treating BLSurf polygon : " << pl_data << std::endl;

        // for each polygon, use the local vertices numbering in ordrer to retrieve the global vertices numbering from bl_vertices
        using vkey = blsurf_frac::vertex_key;
        using vID  = blsurf_frac::vertexID;
        // bl_vertices.find returns an iterator to an element of the map : we keep 'second' which is a reference to a 'vertex_data' structure, and find global_ID from it ; 
        std::vector<vID> vg;
        vg.reserve(num_vertices);
        for(const vID& v : pl_data.vertices)
        {
          const vkey my_key(fractureID,v);
          // const vkey my_key(fractureID,v+1);
          const auto iter = bl_vertices.find(my_key);

          if(iter == bl_vertices.cend())
          {
            const std::string msg = "Error, the following (fracID,vertexID) pair was not found in the list of blsurf vertices : {"
                                    + std::to_string(my_key.first) + ";" + std::to_string(my_key.second) + "}";
            throw std::runtime_error(msg);
          }
          vg.push_back( iter->second.globalID - (vID) 1u );
        }

        // generate diskpp identifiers from those
        std::vector<node_ID> nids;
        nids.reserve(num_vertices);
        for(const vID& v : vg)
          nids.push_back(node_ID(v));
        
        std::set<edge_type> edgs;
        for(size_t i=0; i<nids.size(); i++)
        {
          const node_ID pi = nids[i];
          const node_ID pj = (i==(nids.size()-1)) ? nids[0] : nids[i+1];

          const auto eij = (pi < pj) ? edge_type({pi,pj}) : edge_type({pj,pi}) ;
          edgs.insert(eij);
        }

        for(const edge_type& e: edgs)
          edges.push_back(e);
        
        // store surface as a polygon
        polygon poly;
        // poly.nodes.push_back(p0);
        // poly.nodes.push_back(p1);
        // poly.nodes.push_back(p2);
        poly.nodes = std::move(nids);
        // poly.attached_edges.insert(e0);
        // poly.attached_edges.insert(e1);
        // poly.attached_edges.insert(e2);
        poly.attached_edges = std::move(edgs);
        poly.K = std::move(pl_data.transm_K);

        polygons.push_back(poly);
        
        if(this->verbose())
        {
          std::cout << "BLSurf polygon was registered as :\n" << poly << '\n';
          // std::cout << "Three edges added for this polygon : " << e0 << " | " << e1 << " | "<< e2 << '\n';
        }
      } // end of loop on blsurf triangles
      
      // detect and register boundary edges
      for(const blsurf_frac::poly_BC_edges& e : bl_BC_edgs)
      {
        diskpp_itf::BOUNDARY_FUNC_TYPE dpp_btype = diskpp_itf::BOUNDARY_FUNC_TYPE::NONE;

        // check the type of BC and handle as appropriate
        switch(e.bc_type)
        {
          // case blsurf_frac::poly_BC_edges::NEUMANN_VALUE : 
          case -2 : 
            dpp_btype = diskpp_itf::BOUNDARY_FUNC_TYPE::NEUMANN_ZERO;
          break;

          // case blsurf_frac::poly_BC_edges::DIRICHLET_VALUE :
          case -1 :
          {
            if(e.cube_face == blsurf_frac::cubeFaceID(1))
              dpp_btype = diskpp_itf::BOUNDARY_FUNC_TYPE::DIRICHLET_H1;
            else if(e.cube_face == blsurf_frac::cubeFaceID(2))
              dpp_btype = diskpp_itf::BOUNDARY_FUNC_TYPE::DIRICHLET_H2;
            else
              throw std::runtime_error( std::string( "Error, for a Dirichlet BC the following cube Face is invalid (expected 1 or 2) : "
                                                     + std::to_string(e.cube_face) + " !! " ) 
                                      );
          }
          break;

          default:
            throw std::runtime_error( std::string( "Error, the following BLSurfFrac BC type is invalid : "
                                                   + std::to_string(e.bc_type) + " !! " ) 
                                    );
          break;
        }
        
        if(dpp_btype == diskpp_itf::BOUNDARY_FUNC_TYPE::NONE)
          continue;

        // now register a diskpp BC edge and store it in the boundary_edges data structure
        const auto p0 = node_ID(e.vglo1 - blsurf_frac::vertexID(1u));
        const auto p1 = node_ID(e.vglo2 - blsurf_frac::vertexID(1u));
        const auto bc_edge = (p0 < p1) ? edge_type({p0,p1}) : edge_type({p1,p0});

        boundary_edges.push_back(std::make_pair(bc_edge,dpp_btype));
          
        if(this->verbose())
        {
          std::cout << "Read a boundary edge " << bc_edge << " of ID " << dpp_btype << '\n';
        }

      } // end of loop on blsurf edges
      
      return true;
    }

    template<typename K_map>
    bool populate_mesh(mesh_type& msh, K_map& K)
    {
      auto storage = msh.backend_storage();
      
      if(this->verbose())
      {
        std::cout << "Sorting and moving data to mesh's backend storage ..." << '\n';
      }

      storage->points = std::move(points);
      storage->nodes = std::move(nodes);

      points.clear();
      points.resize(0);

      nodes.clear();
      nodes.resize(0);

      /* sort edges, make unique and move them in geometry */
      disk::priv::sort_uniq(edges);
      storage->edges = std::move(edges);
      edges.clear();
      edges.resize(0);

      // process the polygons, register them as surfaces
      std::vector<surface_type> l_surfaces;
      l_surfaces.reserve( polygons.size() );
      for(const auto& p : polygons)
      {
        std::vector<typename edge_type::id_type> surface_edges;
        for (const auto& e : p.attached_edges)
        {
          const auto edge_id = disk::find_element_id(storage->edges.cbegin(), storage->edges.cend(), e);

          assert(edge_id.first);
          
          // if (!edge_id.first)
          // {
              // std::cout << "Bad bug at " << __FILE__ << "("
                        // << __LINE__ << ")" << std::endl;
              // return false;
          // }

          surface_edges.push_back(edge_id.second);
        }

        auto surface = surface_type(surface_edges);
        surface.set_point_ids(p.nodes.begin(), p.nodes.end());

        K[surface] = p.K;

        l_surfaces.push_back(surface);
      }

      std::sort(l_surfaces.begin(), l_surfaces.end());
      storage->surfaces = std::move(l_surfaces);

      l_surfaces.clear();
      l_surfaces.resize(0);

      polygons.clear();
      polygons.resize(0);

      // migrate boundary edges info to the internal meshh storage
      storage->boundary_info.resize(storage->edges.size());
      for (const auto& be : boundary_edges)
      {
        const auto position = disk::find_element_id(storage->edges.cbegin(), storage->edges.cend(), be.first);
        
        assert(position.first);
        
        const disk::bnd_info bi({be.second, true});
        storage->boundary_info.at(position.second) = bi;
      }
      boundary_edges.clear();
      boundary_edges.resize(0);
 
      if(this->verbose())
      {

        std::cout << "Done !!" << '\n';
        
        std::cout << "Nodes: " << storage->nodes.size() << '\n';
        std::cout << "Edges: " << storage->edges.size() << '\n';
        std::cout << "Surfaces (polygons): " << storage->surfaces.size() << '\n';
        
      }
      
      return true;
    }

  private:

    struct polygon
    {
      std::vector<node_ID>  nodes;
      std::set<edge_type>   attached_edges;

      eigen_Mat_2x2 K;

      // polygon() = default;

      // polygon(std::vector<node_ID>& nds)  {nodes = std::move(nds)};

      // polygon(std::vector<node_ID>&& nds) {nodes = std::move(nds)};

      bool operator<(const polygon& other)
      {
        return nodes < other.nodes;
      }

      friend std::ostream& operator<<(std::ostream& os, const polygon& p)
      {
        os << "Nodes of this polygon : [";
        for(const auto& n : p.nodes)
          os << " " << n << " ";
        os << "]\n";
        os << "Edges of this polygon : [";
        for(const auto& e : p.attached_edges)
          os << " " << e << " ";
        os << "]\n";
        return os;
      }
    };

    std::vector<point_type>                               points;
    std::vector<node_type>                                nodes;
    std::vector<edge_type>                                edges;
    std::vector<std::pair<edge_type,disk::ident_raw_t>>   boundary_edges;

    std::vector<polygon>                                  polygons;

    bool m_verbose;

  }; // end of class custom_polygonal_mesh_loader_from_blsurf_files

  /**
   * @brief TODO
   * 
   * @todo Document this
   * 
   * @tparam Mesh 
   * @tparam StorageIndex 
   */
  template<typename Mesh, typename StorageIndex>
  struct custom_assembler
  {
    using Scalar = typename Mesh::coordinate_type;
    using matrix_type = Eigen::Matrix<Scalar, Eigen::Dynamic, Eigen::Dynamic>;
    using vector_type = Eigen::Matrix<Scalar, Eigen::Dynamic, 1>;
    using boundary_type = disk::scalar_boundary_conditions<Mesh>;

    using triplet_type = Eigen::Triplet<Scalar,StorageIndex>;
    using sparse_matrix_type = Eigen::SparseMatrix<Scalar,Eigen::ColMajor,StorageIndex>;

    std::vector<StorageIndex>                        compress_table;
    std::vector<StorageIndex>                        expand_table;
    disk::hho_degree_info                            di;
    std::vector<triplet_type> triplets;
    bool                                             use_bnd;

    StorageIndex num_all_faces, num_dirichlet_faces, num_other_faces, system_size;

    /**
     * @brief 
     * 
     */
    class assembly_index
    {
      StorageIndex idx;
      bool   assem;

      public:
      assembly_index(StorageIndex i, bool as) : idx(i), assem(as) {}

      operator StorageIndex() const
      {
        if (!assem)
          throw std::logic_error("Invalid assembly_index");

        return idx;
      }

      bool
      assemble() const
      {
        return assem;
      }

      friend std::ostream&
      operator<<(std::ostream& os, const assembly_index& as)
      {
        os << "(" << as.idx << "," << as.assem << ")";
        return os;
      }
    };

    sparse_matrix_type LHS;
    vector_type        RHS;

    // custom_assembler(){};

    /**
     * @brief Construct a new custom assembler object
     * 
     * @param msh 
     * @param hdi 
     */
    custom_assembler(const Mesh& msh, const disk::hho_degree_info& hdi) : di(hdi), use_bnd(false)
    {
      auto is_dirichlet = [&](const typename Mesh::face_type& fc) -> bool { return msh.is_boundary(fc); };

      num_all_faces       = msh.faces_size();
      num_dirichlet_faces = std::count_if(msh.faces_begin(), msh.faces_end(), is_dirichlet);
      num_other_faces     = num_all_faces - num_dirichlet_faces;

      compress_table.resize(num_all_faces);
      expand_table.resize(num_other_faces);

      StorageIndex compressed_offset = 0;
      for (StorageIndex i = 0; i < num_all_faces; i++)
      {
        const auto fc = *std::next(msh.faces_begin(), i);
        if (!is_dirichlet(fc))
        {
          compress_table.at(i)               = compressed_offset;
          expand_table.at(compressed_offset) = i;
          compressed_offset++;
        }
      }

      const auto fbs = disk::scalar_basis_size(hdi.face_degree(), Mesh::dimension - 1);
      system_size    = fbs * num_other_faces;

      LHS = sparse_matrix_type(system_size, system_size);
      RHS = vector_type::Zero(system_size);
    }
    
    /**
     * @brief Construct a new custom assembler object
     * 
     * @param msh 
     * @param hdi 
     * @param bnd 
     */
    custom_assembler(const Mesh& msh, const disk::hho_degree_info& hdi, const boundary_type& bnd) : di(hdi), use_bnd(true)
    {
      auto is_dirichlet = [&](const typename Mesh::face& fc) -> bool
      {
        const auto fc_id = msh.lookup(fc);
        return bnd.is_dirichlet_face(fc_id);
      };

      num_all_faces       = msh.faces_size();
      num_dirichlet_faces = std::count_if(msh.faces_begin(), msh.faces_end(), is_dirichlet);
      num_other_faces     = num_all_faces - num_dirichlet_faces;

      compress_table.resize(num_all_faces);
      expand_table.resize(num_other_faces);

      StorageIndex compressed_offset = 0;
      for (StorageIndex i = 0; i < num_all_faces; i++)
      {
        const auto fc = *std::next(msh.faces_begin(), i);
        if (!is_dirichlet(fc))
        {
          compress_table.at(i)               = compressed_offset;
          expand_table.at(compressed_offset) = i;
          compressed_offset++;
        }
      }

      const auto fbs = disk::scalar_basis_size(hdi.face_degree(), Mesh::dimension - 1);
      system_size    = fbs * num_other_faces;

      LHS = sparse_matrix_type(system_size, system_size);
      RHS = vector_type::Zero(system_size);
    }

    void
    assemble(const Mesh&                     msh,
             const typename Mesh::cell_type& cl,
             const boundary_type&            bnd,
             const matrix_type&              lhs,
             const vector_type&              rhs)
    {
      if (!use_bnd)
        throw std::invalid_argument("diffusion_assembler: you have to use boundary type in the constructor");

      const auto fbs = disk::scalar_basis_size(di.face_degree(), Mesh::dimension - 1);
      const auto fcs = disk::faces(msh, cl);

      std::vector<assembly_index> asm_map;
      asm_map.reserve(fcs.size() * fbs);

      vector_type dirichlet_data = vector_type::Zero(fcs.size() * fbs);

      for (size_t face_i = 0; face_i < fcs.size(); face_i++)
      {
        const auto fc              = fcs[face_i];
        const auto face_offset     = disk::offset(msh, fc);
        const auto face_LHS_offset = compress_table.at(face_offset) * fbs;

        const auto face_id   = msh.lookup(fc);
        const bool dirichlet = bnd.is_dirichlet_face(face_id);

        for (size_t i = 0; i < fbs; i++)
          asm_map.push_back(assembly_index(face_LHS_offset + i, !dirichlet));

        if (dirichlet)
        {
          auto dirichlet_fun = bnd.dirichlet_boundary_func(face_id);
          dirichlet_data.block(face_i * fbs, 0, fbs, 1) = project_function(msh, fc, di.face_degree(), dirichlet_fun, di.face_degree());
        }
      }

      for (size_t i = 0; i < lhs.rows(); i++)
      {
        if (!asm_map[i].assemble())
          continue;

        for (size_t j = 0; j < lhs.cols(); j++)
        {
          if (asm_map[j].assemble())
            triplets.push_back(triplet_type(asm_map[i], asm_map[j], lhs(i, j)));
          else
            RHS(asm_map[i]) -= lhs(i, j) * dirichlet_data(j);
        }

        RHS(asm_map[i]) += rhs(i);
      }
    } // assemble()

    void
    assemble_nefmod(const Mesh&                     msh,
                    const typename Mesh::cell_type& cl,
                    const boundary_type&            bnd,
                    const matrix_type&              lhs,
                    const vector_type&              rhs,
                    std::vector<Eigen::Triplet<Scalar,StorageIndex>>&        perThread_triplets,
                    vector_type&                    perThread_RHS
                    )
    {
      if (!use_bnd)
          throw std::invalid_argument("diffusion_assembler: you have to use boundary type in the constructor");

      const auto fbs = disk::scalar_basis_size(di.face_degree(), Mesh::dimension - 1);
      const auto fcs = disk::faces(msh, cl);

      std::vector<assembly_index> asm_map;
      asm_map.reserve(fcs.size() * fbs);

      vector_type dirichlet_data = vector_type::Zero(fcs.size() * fbs);

      for (size_t face_i = 0; face_i < fcs.size(); face_i++)
      {
          const auto fc              = fcs[face_i];
          const auto face_offset     = disk::offset(msh, fc);
          const auto face_LHS_offset = compress_table.at(face_offset) * fbs;

          const auto face_id   = msh.lookup(fc);
          const bool dirichlet = bnd.is_dirichlet_face(face_id);

          for (size_t i = 0; i < fbs; i++)
              asm_map.push_back(assembly_index(face_LHS_offset + i, !dirichlet));

          if (dirichlet)
          {
              auto dirichlet_fun = bnd.dirichlet_boundary_func(face_id);

              dirichlet_data.block(face_i * fbs, 0, fbs, 1) =
                project_function(msh, fc, di.face_degree(), dirichlet_fun, di.face_degree());
          }
      }

      for (size_t i = 0; i < lhs.rows(); i++)
      {
          if (!asm_map[i].assemble())
              continue;

          for (size_t j = 0; j < lhs.cols(); j++)
          {
              if (asm_map[j].assemble())
              {
                  // #pragma omp critical
                  perThread_triplets.push_back(Eigen::Triplet<Scalar,StorageIndex>(asm_map[i], asm_map[j], lhs(i, j)));
              }
              else
              {
                  // #pragma omp atomic
                  perThread_RHS(asm_map[i]) -= lhs(i, j) * dirichlet_data(j);
              }
          }

          // #pragma omp atomic
          perThread_RHS(asm_map[i]) += rhs(i);
      }
    } // assemble()

    void
    impose_neumann_boundary_conditions(const Mesh& msh, const boundary_type& bnd)
    {
      if (!use_bnd)
        throw std::invalid_argument("diffusion_assembler: you have to use boundary type in the constructor");

      if (bnd.nb_faces_neumann() > 0)
      {
        for (auto itor = msh.boundary_faces_begin(); itor != msh.boundary_faces_end(); itor++)
        {
          const auto bfc     = *itor;
          const auto face_id = msh.lookup(bfc);

          if (bnd.is_neumann_face(face_id))
          {
            if (bnd.is_dirichlet_face(face_id))
            {
              throw std::invalid_argument("You tried to impose"
                                          "both Dirichlet and Neumann conditions on the same face");
            }
            else if (bnd.is_robin_face(face_id))
            {
              throw std::invalid_argument("You tried to impose"
                                          "both Robin and Neumann conditions on the same face");
            }
            else
            {
              const size_t                face_degree   = di.face_degree();
              const size_t                num_face_dofs = disk::scalar_basis_size(face_degree, Mesh::dimension - 1);
              std::vector<assembly_index> asm_map;
              asm_map.reserve(num_face_dofs);

              auto face_offset     = face_id;
              auto face_LHS_offset = compress_table.at(face_offset) * num_face_dofs;

              for (StorageIndex i = 0; i < num_face_dofs; i++)
              {
                asm_map.push_back(assembly_index(face_LHS_offset + i, true));
              }

              auto        fb      = disk::make_scalar_monomial_basis(msh, bfc, face_degree);
              vector_type neumann = disk::make_rhs(msh, bfc, fb, bnd.neumann_boundary_func(face_id), face_degree);

              assert(neumann.size() == num_face_dofs);
              for (StorageIndex i = 0; i < neumann.size(); i++)
              {
                RHS(asm_map[i]) += neumann[i];
              }
            }
          }
        }
      }
      else
        throw std::invalid_argument("There are no Neumann faces");
    }

    /**
     * @brief 
     * 
     * @param msh 
     * @param cl 
     * @param bnd 
     * @param solution 
     * @return vector_type 
     */
    vector_type
    take_local_data(const Mesh&                     msh,
                    const typename Mesh::cell_type& cl,
                    const boundary_type&            bnd,
                    const vector_type&              solution) const
    {
      const auto fbs = disk::scalar_basis_size(di.face_degree(), Mesh::dimension - 1);
      const auto fcs = disk::faces(msh, cl);

      const auto num_faces = fcs.size();

      vector_type ret = vector_type::Zero(num_faces * fbs);

      for (size_t face_i = 0; face_i < num_faces; face_i++)
      {
        const auto fc = fcs[face_i];

        auto eid = disk::find_element_id(msh.faces_begin(), msh.faces_end(), fc);
        if (!eid.first)
            throw std::invalid_argument("This is a bug: face not found");
        const auto face_id = eid.second;

        const bool dirichlet = bnd.is_dirichlet_face(face_id);

        if (dirichlet)
        {
          const auto dirichlet_bf = bnd.dirichlet_boundary_func(face_id);

          ret.block(face_i * fbs, 0, fbs, 1) =
            project_function(msh, fc, di.face_degree(), dirichlet_bf, di.face_degree());
        }
        else
        {
          const auto face_offset             = disk::offset(msh, fc);
          const auto face_SOL_offset         = compress_table.at(face_offset) * fbs;
          ret.block(face_i * fbs, 0, fbs, 1) = solution.block(face_SOL_offset, 0, fbs, 1);
        }
      }

      return ret;
    }

    /**
     * @brief 
     * 
     * @param msh 
     * @param cl 
     * @param bnd 
     * @param solution 
     * @return auto 
     */
    std::pair<vector_type,Eigen::VectorXi>
    take_local_data_nefmod(const Mesh&                     msh,
                           const typename Mesh::cell_type& cl,
                           const boundary_type&            bnd,
                           const vector_type&              solution) const
    {
      const auto fbs    = disk::scalar_basis_size(di.face_degree(), Mesh::dimension - 1);
      const auto fcs    = disk::faces(msh, cl);

      const auto num_faces = fcs.size();

      // assert(fbs == 1);
      // assert(num_faces == 3);

      vector_type ret = vector_type::Zero(num_faces * fbs);
      Eigen::VectorXi ret_ids = Eigen::VectorXi::Zero(num_faces);

      for (StorageIndex face_i = 0; face_i < num_faces; face_i++)
      {
          const auto fc = fcs[face_i];

          auto eid = disk::find_element_id(msh.faces_begin(), msh.faces_end(), fc);
          if (!eid.first)
              throw std::invalid_argument("This is a bug: face not found");
          const auto face_id = eid.second;
          
          ret_ids(face_i) = face_id;

          const bool dirichlet = bnd.is_dirichlet_face(face_id);

          if (dirichlet)
          {
              const auto dirichlet_bf = bnd.dirichlet_boundary_func(face_id);

              ret.block(face_i * fbs, 0, fbs, 1) =
                project_function(msh, fc, di.face_degree(), dirichlet_bf, di.face_degree());
          }
          else
          {
              const auto face_offset             = disk::offset(msh, fc);
              const auto face_SOL_offset         = compress_table.at(face_offset) * fbs;
              ret.block(face_i * fbs, 0, fbs, 1) = solution.block(face_SOL_offset, 0, fbs, 1);
          }
      }

      return std::make_pair(ret,ret_ids);
    }


    vector_type
    take_local_solution(const Mesh& msh,
                        const typename Mesh::cell_type& cl,
                        const boundary_type& bnd,
                        const vector_type& sol) const
    {
      const auto fbs    = disk::scalar_basis_size(di.face_degree(), Mesh::dimension - 1);
      const auto fcs_id    = faces_id(msh, cl);
      const auto num_faces = fcs_id.size();

      vector_type ret = vector_type::Zero(num_faces * fbs);

      for (size_t face_i = 0; face_i < num_faces; face_i++)
      {
        const auto face_id = fcs_id[face_i];

        if (bnd.is_dirichlet_face(face_id))
        {
          const auto dirichlet_bf = bnd.dirichlet_boundary_func(face_id);
          const auto fc           = *std::next(msh.faces_begin(), face_id);
          ret.segment(face_i * fbs, fbs) =
            project_function(msh, fc, di.face_degree(), dirichlet_bf, di.face_degree());
        }
        else
        {
          const auto face_SOL_offset     = compress_table.at(face_id) * fbs;
          ret.segment(face_i * fbs, fbs) = sol.segment(face_SOL_offset, fbs);
        }
      }
      return ret;
   }

    /**
     * @brief 
     * 
     */
    void finalize()
    {
      LHS.setFromTriplets(triplets.begin(), triplets.end());
      triplets.clear();
    }

    void
    finalize_nefmod(std::vector<Eigen::Triplet<Scalar,StorageIndex>>& perThread_triplets, vector_type& perThread_RHS)
    {
      #pragma omp critical
      {
          triplets.insert(triplets.end(), std::make_move_iterator(perThread_triplets.begin()), std::make_move_iterator(perThread_triplets.end()));
          RHS += perThread_RHS;
      }

      #pragma omp barrier 

      #pragma omp master
      {
          LHS.setFromTriplets(triplets.begin(), triplets.end());
          triplets.clear();
          triplets.resize(0);
      }
    }

    /**
     * @brief 
     * 
     * @return size_t 
     */
    size_t num_assembled_faces() const
    {
      return num_other_faces;
    }

  }; // end of class custom_assembler

} // end of namespace diskpp_itf
