/**
 * @file eigen_utils.hpp
 * @author Florent Hédin, Géraldine Pichot (devel@fhedin.com, geraldine.pichot@inria.fr)
 * @brief Tools for Eigen
 * @date 22/10/2020
 * 
 * @copyright INRIA Paris, EPI SERENA, 2020
 * 
 */

#pragma once

#include "config.hpp"

#include <Eigen/Eigenvalues>

namespace eigen_utils
{
  /**
   * @brief Computes the maximum eigenvalue of a Eigen Matrix.
   * 
   * @param m The matrix.
   * 
   * @return The largest of the eigenvalues (real part).
   * 
   */
  inline double compute_max_eigenvalue(const eigen_Mat_2x2& m)
  {
    return double(m.eigenvalues().real().maxCoeff());
  }


  /**
   * @brief This is a custom visitor which is used to obtain the indices of elements of
   *        an Eigen3 matrix (or vector, they are the same with Eigen3) which are not 0.0
   * 
   * @tparam Scalar A scalar type, usually double or float
   */
  template <typename Scalar>
  class getNonZeroIndices
  {
  public:

    /**
     * @brief Called for the first coefficient of the matrix.
     * 
     * @param value Reference to a scalar element of the Eigen::Matrix.
     * @param i Eigen::Matrix row index.
     * @param j Eigen::Matrix col index.
     */
    void init(const Scalar& value, Eigen::Index i, Eigen::Index j)
    {
      if(std::abs(value) > std::numeric_limits<Scalar>::epsilon())
      {
        vals.push_back(value);
        idx_i.push_back(i);
        idx_j.push_back(j);
      }
    }
    
    /**
     * @brief Called for all other coefficients of the matrix.
     * 
     * @param value Reference to a scalar element of the Eigen::Matrix.
     * @param i Eigen::Matrix row index.
     * @param j Eigen::Matrix col index.
     */
    void operator() (const Scalar& value, Eigen::Index i, Eigen::Index j)
    {
      if(abs(value) > std::numeric_limits<Scalar>::epsilon())
      {
        vals.push_back(value);
        idx_i.push_back(i);
        idx_j.push_back(j);
      }
    }

    /**
     * @brief Returns the number of Non Zero elements in the Eigen::Matrix inspected by this visitor.
     
     * @return size_t The number of Non Zero elements.
     */
    size_t get_nnz() const
    {
      assert( idx_i.size() == idx_j.size() );
      assert( idx_i.size() == vals.size() );
      return vals.size();
    }

  private:
  
    std::vector<Eigen::Index> idx_i;
    std::vector<Eigen::Index> idx_j;
    std::vector<Scalar> vals;

  };

}
