
# From powershell : 
#$env:EIGEN3_ROOT_DIR = "$PWD\..\external\eigen"
#$env:MKLROOT = "C:\Program Files (x86)\IntelSWTools\compilers_and_libraries\windows\mkl"
#cmake -G "Visual Studio 16 2019" -A "x64" -DCMAKE_BUILD_TYPE="RelWithDebInfo" -DCMAKE_PREFIX_PATH="C:\Program Files (x86)\swigwin-4.0.2" .. 
$env:EIGEN3_ROOT_DIR = "$PWD\..\NEFpp\external\eigen"
$env:MKLROOT = "C:\dev\mkl_win_redist"
# cmake -G "Visual Studio 16 2019" -A "x64" -T "ClangCL" -DCMAKE_BUILD_TYPE="RelWithDebInfo" -DCMAKE_PREFIX_PATH="C:\Program Files (x86)\swigwin-4.0.2" .. 
cmake -G "Visual Studio 16 2019" -A "x64" -DCMAKE_BUILD_TYPE="RelWithDebInfo" -DCMAKE_PREFIX_PATH="C:\dev\swigwin-4.0.2\" ..\NEFpp 