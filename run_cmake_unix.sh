#!/bin/bash

# source the mklvars.sh file provided by the intel MKL installer ; always target intel64 arch and the lp64 integer model !
# source $HOME/bin/intel/mkl/bin/mklvars.sh intel64 lp64

# on linux clang++ g++ support is guaranteed ; for other O.S. and compilers --> DIY
CXX="clang++-9" EIGEN3_ROOT_DIR="$PWD/../external/eigen" cmake -DCMAKE_BUILD_TYPE="RelWithDebInfo" .. 
