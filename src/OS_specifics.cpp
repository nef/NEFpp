/**
 * @file OS_specifics.cpp
 * @author Florent Hédin, Géraldine Pichot (devel@fhedin.com, geraldine.pichot@inria.fr)
 * @brief This is the file containing code which is system dependent, e.g. using defines such as RUN_ON_LINUX or RUN_ON_WINDOWS
 * @date 22/10/2020
 * 
 * @copyright INRIA Paris, EPI SERENA, 2020
 * 
 */

#include <stdexcept>
#include <map>

#include <iostream>
#include <string>

#include "OS_specifics.hpp"

#ifdef RUN_ON_WINDOWS

#include <direct.h>
#include <windows.h>
#include <psapi.h>
#include <processenv.h>

#define GetCurrentDir _getcwd

#elif ( defined(RUN_ON_LINUX) || defined(RUN_ON_APPLE) || defined(RUN_ON_OTHER_UNIX) )

#include <unistd.h>
#include <sys/resource.h>
#include <sys/utsname.h>

#define GetCurrentDir getcwd

#else
  
// erro
static inline char* bug(char *__buf, size_t __size)
{
  return NULL;
}

#define GetCurrentDir bug

#endif

void dump_eigen_version()
{
  std::cout << "Using Eigen version : " << std::to_string(EIGEN_WORLD_VERSION) + "." +
               std::to_string(EIGEN_MAJOR_VERSION) + "." + std::to_string(EIGEN_MINOR_VERSION) << "\n";
}

void dump_env()
{
#ifndef RUN_ON_WINDOWS
  
  std::map<std::string, std::string> env_vars;
  
  // unistd.h defines : extern char **environ
  int i = 0;
  while(environ[i] != NULL)
  {
    const std::string env_var(environ[i++]);
    const size_t pos = env_var.find_first_of("=");
    const std::pair<std::string,std::string> k_v = {env_var.substr(0,pos),env_var.substr(pos+1,env_var.size())};
    env_vars.insert_or_assign(k_v.first,k_v.second);
  }

  std::cout << "Dump of the environment :\n";
  for (const auto& [k, v] : env_vars)
    std::cout << "\t" + k + " --> " + v << "\n";

#else

  // std::map<std::wstring, std::wstring> env_vars;
  
  // wchar_t* env = GetEnvironmentStringsW();
  // assert(env != NULL);

  // std::wstring env_s(env);
  // std::wcout << env_s << L"\n";

  // while(env_s.size()>0)
  // {
  //   // std::cout << "env_s.size() : " << env_s.size() << "\n";

  //   size_t pos = env_s.find_first_of(L' ');
  //   const std::wstring k = env_s.substr(0,pos);
  //   env_s.erase(0,pos+1);

  //   pos = env_s.find_first_of(L' ');
  //   const std::wstring v = env_s.substr(0,pos);
  //   env_s.erase(0,pos+1);

  //   env_vars.insert_or_assign(k,v);

  // }
  
  std::cerr << "Dump of the environment disabled on windows, continuing...\n";

  // auto res = FreeEnvironmentStringsW(env);
  // assert(res != 0);

  // std::wcout << L"Dump of the environment :\n";
  // for (const auto& [k, v] : env_vars)
  //   std::wcout << std::wstring(L"\t" + k + L" --> " + v + L"\n");

#endif  
}

std::string get_OS_name()
{
  std::string name("Unknown OS");
  
  #ifdef RUN_ON_WINDOWS
  
  OSVERSIONINFO info;
  ZeroMemory(&info, sizeof(OSVERSIONINFO));
  info.dwOSVersionInfoSize = sizeof(OSVERSIONINFO);
  
  const auto ret = GetVersionEx(&info);
  // On windows a return value of 0 is an error !!!
  if(ret == 0)
    throw std::runtime_error("Error when calling the Windows function 'GetVersionEx' !!");
  
  name = "Windows version: " + std::to_string(info.dwMajorVersion) + "." + std::to_string(info.dwMinorVersion) ;

  #elif ( defined(RUN_ON_LINUX) || defined(RUN_ON_APPLE) || defined(RUN_ON_OTHER_UNIX) )

  struct utsname data;
  
  const int ret = uname(&data);
  
  if(ret != 0)
    throw std::runtime_error("Error when calling the POSIX function 'int uname(struct utsname *name)' !!");

  name = std::string(data.sysname) + " " + std::string(data.nodename) + " " + std::string(data.release) + " " + std::string(data.version) + " " + std::string(data.machine);
  
  #endif
  
  return name;
}

std::string get_current_dir()
{
  char path[FILENAME_MAX] = "";
  
  auto ret = GetCurrentDir(path,FILENAME_MAX);
  if(ret == NULL)
    throw std::runtime_error("Error while retrieving the current directory name !!");
  
  return std::string(path);
}

bool is_null_device(const std::string& fname)
{
  #ifdef RUN_ON_WINDOWS
  const bool test = (fname == "nul");
  #else
  const bool test = (fname == "/dev/null");
  #endif

  return test;
}

/**
 * @brief Returns the peak (maximum so far) resident set size RSS (physical
 *        memory use) measured in bytes, or zero if the value cannot be
 *        determined on this OS.
 * 
 * @return The RSS in Bytes.
 */
static inline size_t getPeakRSS()
{
  #if defined(RUN_ON_WINDOWS)

  /* Windows -------------------------------------------------- */
  PROCESS_MEMORY_COUNTERS info;
  GetProcessMemoryInfo( GetCurrentProcess( ), &info, sizeof(info) );
  return (size_t)info.PeakWorkingSetSize;

  #elif (defined(RUN_ON_LINUX) || defined(RUN_ON_APPLE))

  /* BSD, Linux, and OSX -------------------------------------- */
  struct rusage rusage;
  getrusage( RUSAGE_SELF, &rusage );

    #if defined(RUN_ON_APPLE)
      return (size_t)rusage.ru_maxrss;
    #else
      return (size_t)(rusage.ru_maxrss * 1024L);
    #endif
  
  #else
  /* Unknown OS ----------------------------------------------- */
  std::cerr << "getPeakRSS() unsupported on your O.S. , continuing...\n";
  return (size_t)0L;          /* Unsupported. */
  #endif
}

double getPeakRSS_kiB()
{
  constexpr double i_kilo = double(1.0/1024.0);

  return double(getPeakRSS() * i_kilo);
}

double getPeakRSS_MiB()
{
  constexpr double i_mega = double(1.0/(1024.0*1024.0));
  return double(getPeakRSS() * i_mega);
}

double getPeakRSS_GiB()
{
  constexpr double i_giga = double(1.0/(1024.0*1024.0*1024.0));
  return double(getPeakRSS() * i_giga);
}
