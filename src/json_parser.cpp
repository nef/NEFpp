/**
 * @file json_parser.cpp
 * @author Florent Hédin, Géraldine Pichot (devel@fhedin.com, geraldine.pichot@inria.fr)
 * @brief This file contains a function parsing the JSON input file provided by the user ; it uses the LIT licensed nlohmann::json header only library, provided in the "external" directory.
 *
 * @date 22/10/2020
 * 
 * @copyright INRIA Paris, EPI SERENA, 2020
 * 
 */

#include <iostream>
#include <fstream>
#include <set>

#include "config.hpp"

#include "OS_specifics.hpp"
#include "json_parser.hpp"

#include "data_structs.hpp"

#include "json.hpp"

using namespace std;
using json = nlohmann::json;

/*
 * String representation of each solver, should match the order of the struct Available_SparseSolvers
 * found in file 'data_structs.hpp' ; this is local data.
 */
static const auto supported_solvers = {
  "EigenSparseLU",
  "EigenSimplicialLLT"
#ifdef HAVE_INTEL_MKL
  ,"IntelPardisoLU"
  ,"IntelPardisoLLT"
  ,"IntelPardisoLDLT"
#endif
};

input_param_t parse_json_file(const string& fname, bool verbose, bool minimum_IO)
{
  input_param_t inp;

  // read a JSON file
  ifstream str(fname);
  json parser;
  str >> parser;
  str.close();
  
  // keep trace of the keys found in the json data structure, we will remove them progressively when the parsing is successful.
  set<string> keys_found;
  
  if(!minimum_IO)
  {
    cout << "Dumping parsed json content : " << '\n';
    for(const auto& [key,value] : parser.items())
    {
      cout << "json: [key --> value] = " << key << " --> " << value << '\n';
      keys_found.insert(key);
    }
  }
  
  // catch errors nicely if parameter not found in json object while using .at()
  try
  {
    // -------------------------------------------------------------------------------------------------------------------------------
    // first take care of parsing mandatory parameters : if a required parameter is not found the ".at()" will throw an exception.
    // -------------------------------------------------------------------------------------------------------------------------------

    inp.blsurf_mesh_directory = parser.at("blsurf_mesh_directory").get<string>();
    keys_found.erase("blsurf_mesh_directory");

    inp.Lx = parser.at("Lx").get<double>();
    keys_found.erase("Lx");

    inp.Ly = parser.at("Ly").get<double>();
    keys_found.erase("Ly");

    inp.Lz = parser.at("Lz").get<double>();
    keys_found.erase("Lz");

    inp.polynomial_order_k = parser.at("polynomial_order_k").get<size_t>();
    keys_found.erase("polynomial_order_k");

    inp.h1 = parser.at("h1").get<double>();
    keys_found.erase("h1");
    
    inp.h2 = parser.at("h2").get<double>();
    keys_found.erase("h2");

    const string desired_solver = parser.at("solver").get<string>();
    
    if(desired_solver == "EigenSparseLU")
    {
      inp.solver_type = Available_SparseSolvers::EigenSparseLU;
      cout << "Using solver EigenSparseLU" << '\n';
    }
    else if(desired_solver == "EigenSimplicialLLT")
    {
      inp.solver_type = Available_SparseSolvers::EigenSimplicialLLT;
      cout << "Using solver EigenSimplicialLLT" << '\n';
    }
#ifdef HAVE_INTEL_MKL
    else if(desired_solver == "IntelPardisoLU")
    {
      inp.solver_type = Available_SparseSolvers::IntelPardisoLU;
      cout << "Using solver IntelPardisoLU" << '\n';
    }
    else if(desired_solver == "IntelPardisoLLT")
    {
      inp.solver_type = Available_SparseSolvers::IntelPardisoLLT;
      cout << "Using solver IntelPardisoLLT" << '\n';
    }
    else if(desired_solver == "IntelPardisoLDLT")
    {
      inp.solver_type = Available_SparseSolvers::IntelPardisoLDLT;
      cout << "Using solver IntelPardisoLDLT" << '\n';
    }
#endif
    else
    {
      clog << "ATTENTION : for the keyword 'solver' you specified '" << desired_solver << "' which is currently unsupported !! " << '\n';

      clog << "Please use one of the following keywords for the 'solver' field : ";
      for(const auto& str : supported_solvers)
      {
        clog << str << " ";
      }
      clog << '\n';

      clog << "Program continues using the default 'EigenSimplicialLLT'..." << '\n';

      inp.solver_type = Available_SparseSolvers::EigenSimplicialLLT;
    }
    keys_found.erase("solver");

    inp.hdf5_output = parser.at("hdf5_output").get<bool>();
    keys_found.erase("hdf5_output");

    // -------------------------------------------------------------------------------------------------------
    // Then parse optional parameters : we first check if parameters were provided using "contains"
    // -------------------------------------------------------------------------------------------------------
    if(parser.contains("use_transm_file"))
    {
      inp.has_transm_file = parser.at("use_transm_file").get<bool>();
      keys_found.erase("use_transm_file");
    }

    if(inp.has_transm_file)
    {
      inp.transm_file_name = parser.at("transm_file_name").get<string>();
      keys_found.erase("transm_file_name");

      inp.transm_fracIDs_mapping = parser.at("transm_fracIDs_mapping").get<string>();
      keys_found.erase("transm_fracIDs_mapping");
    }
    else
    {
      inp.Txx = parser.at("Txx").get<double>();
      keys_found.erase("Txx");
      
      inp.Tyy = parser.at("Tyy").get<double>();
      keys_found.erase("Tyy");
      
      inp.Txy = parser.at("Txy").get<double>();
      keys_found.erase("Txy");
    }

    if(inp.hdf5_output)
    {
      inp.hdf5_output_fname = parser.at("hdf5_output_fname").get<string>();
      keys_found.erase("hdf5_output_fname");
    }

  } // end try
  catch(exception& e)
  {
    const string forward_message = "Exception when parsing the input file '" + fname + "' : " + e.what();
    throw runtime_error(forward_message);
  }
  
  // if the user defined unused key/values 
  for(const string& k : keys_found)
  {
    clog << "ATTENTION : the keyword '" << k << "' found in the JSON input file '"<< fname << "' was IGNORED !" << '\n';
  }

  return inp;
}
