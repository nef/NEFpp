################################################################################
#
# \file      cmake/FindMKL.cmake
# \author    J. Bakosi
# \copyright 2012-2015, Jozsef Bakosi, 2016, Los Alamos National Security, LLC.
# \brief     Find the Math Kernel Library from Intel
# \date      Thu 26 Jan 2017 02:05:50 PM MST
#
################################################################################

# Find the Math Kernel Library from Intel
#
#  MKL_FOUND - System has MKL
#  MKL_INCLUDE_DIRS - MKL include files directories
#  MKL_RT_LIBRARY - MKL run time library
#
#  The environment variables MKLROOT and INTEL are used to find the library.
#  Everything else is ignored. If MKL is found "-DMKL_ILP64" is added to
#  CMAKE_C_FLAGS and CMAKE_CXX_FLAGS.
#
#  Example usage:
#
#  find_package(MKL)
#  if(MKL_FOUND)
#    target_link_libraries(TARGET ${MKL_LIBRARIES})
#  endif()

set(RT_LIB "mkl_rt")

find_path(MKL_INCLUDE_DIR
          NAMES "mkl.h"
          HINTS $ENV{MKLROOT}/include
          HINTS /usr/include/mkl
          REQUIRED
)

find_library(MKL_RT_LIBRARY
             NAMES ${RT_LIB}
             PATHS $ENV{MKLROOT}/lib
                   $ENV{MKLROOT}/lib/intel64
                   $ENV{INTEL}/mkl/lib/intel64
                   REQUIRED
            )

set(MKL_INCLUDE_DIRS ${MKL_INCLUDE_DIR})

message(STATUS "MKLROOT is : $ENV{MKLROOT}")
message(STATUS "MKL_INCLUDE_DIRS is : ${MKL_INCLUDE_DIRS}")
message(STATUS "MKL_RT_LIBRARY is : ${MKL_RT_LIBRARY}")

# Handle the QUIETLY and REQUIRED arguments and set MKL_FOUND to TRUE if
# all listed variables are TRUE.
INCLUDE(FindPackageHandleStandardArgs)

FIND_PACKAGE_HANDLE_STANDARD_ARGS(MKL DEFAULT_MSG MKL_INCLUDE_DIRS MKL_RT_LIBRARY)

MARK_AS_ADVANCED(MKL_INCLUDE_DIRS MKL_RT_LIBRARY)