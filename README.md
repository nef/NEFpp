# NEFpp : A hybrid high-order (HHO) method for studying flows in discrete fracture networks using non-matching meshes.

This software is made available from the following Inria gitlab repository (access restrictions may apply):

https://gitlab.inria.fr/nef/NEFpp

The following links provide pre-built binary versions: an Intel CPU with AVX and AVX2 instructions is **REQUIRED** : 

+ Windows 10 : this archive contains an autonomous python3 distribution including the NEFpp module and all the dependencies:
https://mybox.inria.fr/f/f56ff775c5ee403ab8e8/?dl=1

+ Linux : No precompiled version available for now ; please follow instructions below, compiling on Linux is quite easy.

## Cloning using git

Clone this git repository or download a source archive from the gitlab webpage of this repository ; then check the **get_deps_if_required.sh**
script which will download the DiSk++ end Eigen3 dependencies to the external folder (if your OS can't execute a sh script, download the git dependencies yourself in external).

## Compiling the code

The two supported platforms are either **linux Ubuntu + Clang** or **Windows 10 + Clang (provided with Visual Studio 2019)** ; 
other combinations of OS + Compiler may work but are untested and no support is provided.

The following are required dependencies that you should install before attempting to compile the software:

+ **CMake** is required on all platforms for building the software ; try to install it using a package manager (or via a manual download: https://cmake.org/download/), as compiling it is quite time consuming; version 3.16 or newer required.

+ **Intel MKL** : On some ubuntu releases one can install from the repos using: **sudo apt install libmkl-dev** ; another possibility is to perform a manual install from Intel's website: https://software.intel.com/content/www/us/en/develop/tools/math-kernel-library.html ; tested with versions 2018 and 2019.

+ **SWIG** : Install from a repo or download from the official website: http://www.swig.org/download.html ; requires version 4.0 or newer.

+ **Python 3** distribution and the following packages : **numpy**, **h5py** ; see OS specific info below.

+ A C++ compiler, at least capable of generating code according to the C++17 standard (see https://en.wikipedia.org/wiki/C%2B%2B17#Compiler_support) ; below are listed development platforms (OS + compiler toolset) on which the software was successfully built.

## MS Windows

Windows 10 was tested (version 1909 OS BUild 18363.1016).

+ First download the Visual Studio Community Installer (download: https://visualstudio.microsoft.com/fr/vs/community/).

+ From the installer select the Visual Studio Community Edition 2019 (v16.7.6 tested).

+ From the main panel, install the C++ and Python development environment (see the first screen capture below).

![VS2019_base](doc/install_procedure/1.png)

+ From the right side panel, under the Python 3 section, check that **Python 3 64 bits** and **Python miniconda** are selected (see the second screen capture below).
![VS2019_python3](doc/install_procedure/2.png)

+ From the right side panel, under the C++ development section, check that **CMake for Windows** and **Clang tools for Windows** are selected.

![VS2019_VC++_Clang](doc/install_procedure/3.png)

+ Proceed to the installation.

Once the installation procedure is completed:

+ Download MKL redistributable dlls from the following INRIA MyBox link (https://mybox.inria.fr/f/8a7cf4dc9fcf43228baf/?dl=1) and extract the **mkl_win_redist** directory it contains to the root of this project.

+ Open **Visual Studio 2019** ; go to the **Tools -> Python -> Python Environments** section ; install the **numpy** and **h5py** packages (see screen captures below).

![VS2019_py3env](doc/install_procedure/3_1.png)
![VS2019_py3packages](doc/install_procedure/3_2.png)
![VS2019_py3packages](doc/install_procedure/3_3.png)

+ Open a **Developer Powershell for VS2019** terminal, cd to the directory where you cloned this git repository, create a **build** directory and enter
this directory.

![PS_VS2019](doc/install_procedure/4.png)

+ Execute the **run_cmake_win64.ps1** powershell script from the terminal or copy paste its content to the terminal ; adjust yourself the **$env:MKLROOT** so that they point to your installation directories of Intel MKL ; adjust also the **-DCMAKE_PREFIX_PATH** parameter so that it points to the install directory of SWIG.

+ A visual studio solution file **NEFpp.sln** is generated ; open it with Visual Studio 2019 ; if not done yet select the **RelWithDebInfo** and **x64** targets (see screen capture below).

![PS_VS2019](doc/install_procedure/5.png)

+ From the solution explorer on the left, select the **NEFpp** subproject of the solution, right-click and open **Properties** ; set the **Platform toolset** property to **LLVM (clang-cl)** (see screen capture below).

![PS_VS2019](doc/install_procedure/6.png)

+ You can now build the **NEFpp** solution.

+ After success, right-click on the **INSTALL** subproject of the solution and click on Build : this will copy the Python 3 NEFpp library and the required MKL dependencies to your Python 3 user site-packages, e.g. **C:\Users\myUserName\AppData\Roaming\Python\Python37\site-packages/NEFpp**

+ The NEFpp library needs in particular the following DLL: mkl_rt.dll and python37.dll, you might have to add the path of these DLL to your environment variable PATH.

+ Now open a Command prompt and execute the following command : **C:\Program Files (x86)\Microsoft Visual Studio\Shared\Python37_64\python.exe -c "import NEFpp; NEFpp.dump_eigen_version();"** : on success you should see printed to the terminal the version of the Eigen library (currently **3.3.7**) as below:

![CMD_NEFpp_OK](doc/install_procedure/7.png)

## Linux Ubuntu 18.04 or 20.04 (x64)

On a recent Ubuntu release (18.04 and 20.04 tested) execute the following commands to install the development environment: 

+ **sudo apt install cmake**
+ **sudo apt install python3 libpython3-dev python3-numpy python3-h5py**
+ **sudo apt install clang-9** or **sudo apt install clang-10** (you can also install and use g++ v9 or g++ v10)

If your release provides a repo containing the Intel MKL you can try:

+ **sudo apt install libmkl-dev**

If not available install Intel MKL manually with the links provided at the top of this document.

Then create a **build** directory and enter this directory then execute:

+ **bash ../run_cmake_unix.sh** (after modifying it if necessary).
+ **make** : this will compile the code, use **make -j N** to compile in parallel N files at the same time.
+ **make test && make install**: this will perform a few non-regression test and install the python 3 module on success : file are installed to your
Python 3 user site-packages, e.g. **/home/$USER/.local/lib/python3.6/site-packages**.

For other Linux distibutions the procedure is similar although you will have to find how to install the packages yourself.

## Running a simulation

For running a simulation please use the **run_NEFpp.py** python file provided at the root of this repository: one argument is expected,
the path to a json input file.

See the ci directory and particularly the files ci/L5_nc_transm_const.json and ci/L5_nc_transm_per_frac.json

Example : cd to the **ci** directory and run:

**python3 ../run_NEFpp.py ./L5_nc_transm_const.json**

This will run in the **ci** dir a small test case used for continuous integration.

Be sure that the path to the mesh input file as defined in the input json file is correct ; also check that the names of the files (e.g. **polygons.vector**) are correct: nothing is hard coded you can change the names either in the python script or in the json file.

## Generating the API documentation (developer doc)

Install Doxygen and from the current directory (containing Doxyfile) run **doxygen** : this will generate a html documentation in doc/html that you can use wih your favorite web browser, e.g. **firefox doc/html/index.html**

A latex documentation is also available in doc/latex, but you will have to compile it yourself using the automatically generated
doc/latex/Makefile : it requires the DOT executable in your path, usually provided by the graphviz package (on ubuntu: **sudo apt install graphviz**).

## Continuous Integration (CI)

[![pipeline status](https://gitlab.inria.fr/nef/NEFpp/badges/swig_full/pipeline.svg)](https://gitlab.inria.fr/nef/NEFpp/-/commits/swig_full)

[![coverage report](https://gitlab.inria.fr/nef/NEFpp/badges/swig_full/coverage.svg)](https://gitlab.inria.fr/nef/NEFpp/-/commits/swig_full)

The **h5diff** tool (provided with standard HDF5 installations) is used when performing a **make test** for comparing to reference simulations provided in
./ci/L5_ref_h5

## License

NEFpp is licensed under the GPLv3: https://www.gnu.org/licenses/gpl-3.0.en.html.