#!/usr/bin/env python3

import h5py

import numpy as np

fw = h5py.File("mytestfile.h5", "w")

my_np_type = np.dtype([('r', np.int64), ('c', np.int64), ('v',np.float64)])

test_array = np.array( [(1,1,-1.0), (0,0,0.0)], dtype = my_np_type )

fw.create_dataset(name = "test_array", dtype = my_np_type, data = test_array)

fw.close()

import os
os.remove("mytestfile.h5")
