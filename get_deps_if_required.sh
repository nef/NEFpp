#!/bin/bash

if [ ! -d "external/diskpp" ]; then
    git clone -b "for_nef_polygons_new" --single-branch --depth 1 git@gitlab.inria.fr:nef/diskpp.git external/diskpp
fi

if [ ! -d "external/eigen" ]; then
    git clone -b "3.3.7" --single-branch --depth 1 https://github.com/eigenteam/eigen-git-mirror.git external/eigen
    # git clone -b "3.3.8-rc1" --single-branch --depth 1 https://gitlab.com/libeigen/eigen.git external/eigen
fi

