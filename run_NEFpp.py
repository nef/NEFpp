#!/usr/bin/env python3

import sys
import math
import os

import numpy
import h5py

import time
import datetime

import NEFpp

if len(sys.argv) != 2:
  raise ValueError("Please provide the path to the json file as argument !")

# decide if the prog will be verbose or reduce its output to a minimum ; both are mutually exclusive
verbose = False
#verbose = True

minimum_IO = False
# minimum_IO = True

# verboseSolver = True
verboseSolver = False

# Whether to perform the assembling in parallel or not ; only if code compiled with -fopenmp, experimental and possibly inducing precision loss 
wants_omp_assembler = False

# if using Pardiso LU/LLT/LDLT as solver, the following environment variables can be used for enabling or disabling preconditioning using the CGS/CG algorithm.
# set to 0 or do not define this variable for disabling the preconditioner
# see the Pardiso documentation (https://software.intel.com/en-us/mkl-developer-reference-c-pardiso-iparm-parameter) for details on the accepted values.
# os.environ["NEFPP_PARDISO_PRECONDITIONER"] = "0"
# os.environ["NEFPP_PARDISO_PRECONDITIONER"] = "31" # LU-preconditioned CGS iteration with a stopping criterion of 1.0E-3 for nonsymmetric matrices
os.environ["NEFPP_PARDISO_PRECONDITIONER"] = "61" # LU-preconditioned CGS iteration with a stopping criterion of 1.0E-6 for nonsymmetric matrices
# os.environ["NEFPP_PARDISO_PRECONDITIONER"] = "32" # LLT-preconditioned CGS iteration with a stopping criterion of 1.0E-3 for symmetric positive definite matrices
# os.environ["NEFPP_PARDISO_PRECONDITIONER"] = "62" # LLT-preconditioned CGS iteration with a stopping criterion of 1.0E-6 for symmetric positive definite matrices

# when running on a cpu with AVX-512 instructions it may happen that perfs are worse than expected
# see : https://community.intel.com/t5/Software-Tuning-Performance/AVX512-slower-than-AVX2-with-Intel-MKL-dgemm-on-Intel-Gold-5118/td-p/1135951
# and : https://software.intel.com/content/www/us/en/develop/documentation/mkl-developer-reference-c/top/support-functions/miscellaneous/mkl-enable-instructions.html
# use the following to force the use of an "older" instruction set like AVX2 AVX or SSE
# os.environ["MKL_ENABLE_INSTRUCTIONS"] = "AVX2"
# os.environ["MKL_ENABLE_INSTRUCTIONS"] = "SSE4_2"
# os.environ["MKL_ENABLE_INSTRUCTIONS"] = "AVX"

NEFpp.dump_eigen_version()
NEFpp.dump_env()

#inp = NEFpp.parse_json_file("L5/NonConform/k0.json",verbose,minimum_IO)
start_time = time.time()

inp = NEFpp.parse_json_file(sys.argv[1],verbose,minimum_IO)

print("mesh directory is : %s" % inp.blsurf_mesh_directory)

# define path to mesh input files
f_polygons = inp.blsurf_mesh_directory + NEFpp.get_path_separator() + "polygons.vector"
f_vertices = inp.blsurf_mesh_directory + NEFpp.get_path_separator() + "polygons_vertices.vector"
f_BC_edges = inp.blsurf_mesh_directory + NEFpp.get_path_separator() + "polygons_BC_edges.vector"

f_transm = (inp.blsurf_mesh_directory + NEFpp.get_path_separator() + inp.transm_file_name) if inp.has_transm_file else NEFpp.get_null_device_name()
f_transm_frac = (inp.blsurf_mesh_directory + NEFpp.get_path_separator() + inp.transm_fracIDs_mapping) if inp.has_transm_file else NEFpp.get_null_device_name()

# read the blsurf_frac data files ; note the bl_* are opaque pointers to c++ data structures not intended to be read/altered from the Python side !
#  this is because allowing complete access to all the DiSk++ methods and data structures would be too much work (and probably overkill)
bl_vertices = NEFpp.read_vertices(f_vertices)
bl_edges    = NEFpp.read_edges(f_BC_edges)
bl_polygons = NEFpp.read_polygons(f_polygons)
NEFpp.read_transmissivity(inp,f_transm,f_transm_frac,bl_polygons)

# build the diskpp_mesh using the previously read blsurf_frac definitions.
# after a call to this function the mesh data resides in diskpp_mesh, and the bl_* are empty
# once again diskpp_mesh is an opaque c++ data structure not intended to be read/altered from the Python side
diskpp_mesh = NEFpp.build_mesh(bl_vertices,bl_edges,bl_polygons,verbose,minimum_IO)

# force memory cleaning ?
bl_vertices = 0
bl_edges = 0
bl_polygons = 0

# this returns a std::vector<long long int>
dpp_vertices = NEFpp.get_diskpp_vertices(diskpp_mesh,verbose,minimum_IO)

# this returns a std::vector<long long int>
dpp_edges = NEFpp.get_diskpp_edges(diskpp_mesh,verbose,minimum_IO)

# this returns a std::vector<long long int>
dpp_polygons_vids = NEFpp.get_diskpp_polygons_as_vertices_ids(diskpp_mesh,verbose,minimum_IO)
dpp_polygons_eids = NEFpp.get_diskpp_polygons_as_edges_ids(diskpp_mesh,verbose,minimum_IO)

# this returns 2 long long int
dpp_polygons_info_pair = NEFpp.get_diskpp_polygons_sizes(diskpp_mesh,verbose,minimum_IO)
dpp_num_polygons = dpp_polygons_info_pair.first
dpp_num_of_polygons_max_vertices = dpp_polygons_info_pair.second

# build the diskpp Boundary Conditions object ; again an opaque c++ data structure not intended to be read/altered from the Python side
diskpp_bc = NEFpp.build_boundary_conditions(inp,diskpp_mesh,verbose,minimum_IO)

# get details on the type of bc and its value
vector_pair_bcid_bcval = NEFpp.get_bc_type_and_value(diskpp_mesh,diskpp_bc,verbose,minimum_IO)

# set the HHO degrees from the polynomial order
hho_cell_degree = inp.polynomial_order_k + 1
hho_face_degree = inp.polynomial_order_k
hho_grad_degree = inp.polynomial_order_k
# build the hho object and print details
hho_deg_info = NEFpp.hho_degree_info(hho_cell_degree,hho_face_degree,hho_grad_degree)
hho_deg_info.info_degree()

# assemble the system ; 64 bits integers are used for the indices of the sparse matrix, large systems are therefore handled

assembler = NEFpp.assemble_system(diskpp_mesh,hho_deg_info,diskpp_bc,verbose,minimum_IO)

# access to the sparse matrix from python
size_LS = NEFpp.get_sparse_matrix_size(assembler,verbose,minimum_IO)
sp_mat = NEFpp.get_sparse_matrix_coo(assembler,verbose,minimum_IO)

# solve the system ; solver used depends of the value of inp.solver_type
residual = NEFpp.solve_system(inp,assembler,verboseSolver,minimum_IO)

# compute flux and other properties ;
# Below the content of the calc_props structure : 
#  struct nef_properties
#  {
#     Nefpp_scalar Q1, Q2;
#     std::vector<Nefpp_scalar> flux_per_edge, area_of_cell, press_per_cell, Tp_per_edge;
#  };
#
calc_props = NEFpp.compute_properties(diskpp_mesh,diskpp_bc,hho_deg_info,assembler,verbose,minimum_IO)

# for the following elements it is necessary to free memory
diskpp_mesh = NEFpp.delete_diskpp_mesh(diskpp_mesh)
diskpp_bc = NEFpp.delete_diskpp_bc(diskpp_bc)
assembler = NEFpp.delete_diskpp_asm(assembler)

print("Q1 = %e" % calc_props.Q1)
print("Q2 = %e" % calc_props.Q2)
Qdiff = math.fabs(calc_props.Q2 + calc_props.Q1)

print("Qdiff = %e" % Qdiff)

if Qdiff > 1e-7:
  print("ATTENTION ! Mass conservation problem because Qdiff > 1e-7 !!!!")

delta_h = inp.h1 - inp.h2
equ_perm = - calc_props.Q1*inp.Lx/(inp.Ly*inp.Lz*delta_h)

print("Equivalent Permeability = %e" % equ_perm)
print("Total time: %s " % str(datetime.timedelta(seconds=round(time.time() - start_time))))

fichier = open("results.txt", "a")
fichier.write('k='+str(hho_face_degree)+'\n')
fichier.write(str(calc_props.Q1).replace('.',',')+ '\n')
fichier.write(str(calc_props.Q2).replace('.',',')+ '\n')
fichier.write(str(Qdiff).replace('.',',')+ '\n')
fichier.write(str(equ_perm).replace('.',',')+ '\n')
fichier.write(str(datetime.timedelta(seconds=round(time.time() - start_time)))+ '\n')
fichier.write(str(NEFpp.getPeakRSS_MiB()).replace('.',',')+ '\n')
fichier.write(str(size_LS)+'\n')
fichier.write('\n')
fichier.close()

if(inp.hdf5_output_fname == False):
  print("HDF5 output disabled, now exiting after successfull computation...")
  sys.exit(0)

# HDF5 output of properties of interest goes here
h5f = h5py.File(inp.hdf5_output_fname, "w")

print("Now writing the HDF5 output file to '%s' with properties of interest..." % inp.hdf5_output_fname)

# For some of the datasets below we create a "units" attribute for keeping trace of the physical unit used in the computation ;
#  the following "units_scheme" points to the URL of a proposed addition to the HDF5 standard with more details.
h5f.attrs["units_scheme"] = "https://github.com/takluyver/hdf5-units/blob/97cf8a0/specification.md"

# check the python h5py package doc here for details on the compression, shuffle and fletcher32-crc parameters : https://docs.h5py.org/en/stable/

# mesh and geometry data
msh_grp = h5f.create_group("mesh_data")

msh_grp.create_dataset(name = "area_of_cell",
                       shape = (1,calc_props.area_of_cell.size()),
                      #  chunks = True,
                       compression = "gzip",
                      #  shuffle = True,
                      #  fletcher32 = True,
                       dtype='float64',
                       data = calc_props.area_of_cell)

msh_grp["area_of_cell"].attrs["units"] = "m2"

msh_grp.create_dataset(name = "mT",
                       shape = (1,calc_props.mT.size()),
                      #  chunks = True,
                       compression = "gzip",
                      #  shuffle = True,
                      #  fletcher32 = True,
                       dtype='float64',
                       data = calc_props.mT)

msh_grp["mT"].attrs["units"] = "m"

msh_grp.create_dataset(name = "hT",
                       shape = (1,calc_props.hT.size()),
                      #  chunks = True,
                       compression = "gzip",
                      #  shuffle = True,
                      #  fletcher32 = True,
                       dtype='float64',
                       data = calc_props.hT)

msh_grp["hT"].attrs["units"] = "m"

msh_grp.create_dataset(name = "qualityTetra",
                       shape = (1,calc_props.qualityTetra.size()),
                      #  chunks = True,
                       compression = "gzip",
                      #  shuffle = True,
                      #  fletcher32 = True,
                       dtype='float64',
                       data = calc_props.qualityTetra)

msh_grp["qualityTetra"].attrs["units"] = "dimensionless"

msh_grp.create_dataset(name = "qualitySubdivTri_max",
                       shape = (1,calc_props.qualitySubdivTri_max.size()),
                      #  chunks = True,
                       compression = "gzip",
                      #  shuffle = True,
                      #  fletcher32 = True,
                       dtype='float64',
                       data = calc_props.qualitySubdivTri_max)

msh_grp["qualitySubdivTri_max"].attrs["units"] = "dimensionless"

msh_grp.create_dataset(name = "qualitySubdivTri_min",
                       shape = (1,calc_props.qualitySubdivTri_min.size()),
                      #  chunks = True,
                       compression = "gzip",
                      #  shuffle = True,
                      #  fletcher32 = True,
                       dtype='float64',
                       data = calc_props.qualitySubdivTri_min)

msh_grp["qualitySubdivTri_min"].attrs["units"] = "dimensionless"

msh_grp.create_dataset(name = "hF_max",
                       shape = (1,calc_props.hF_max.size()),
                      #  chunks = True,
                       compression = "gzip",
                      #  shuffle = True,
                      #  fletcher32 = True,
                       dtype='float64',
                       data = calc_props.hF_max)

msh_grp["hF_max"].attrs["units"] = "m"

msh_grp.create_dataset(name = "hF_min",
                       shape = (1,calc_props.hF_min.size()),
                      #  chunks = True,
                       compression = "gzip",
                      #  shuffle = True,
                      #  fletcher32 = True,
                       dtype='float64',
                       data = calc_props.hF_min)

msh_grp["hF_min"].attrs["units"] = "m"

msh_grp.create_dataset(name = "hR_min",
                       shape = (1,calc_props.hR_min.size()),
                      #  chunks = True,
                       compression = "gzip",
                      #  shuffle = True,
                      #  fletcher32 = True,
                       dtype='float64',
                       data = calc_props.hR_min)

msh_grp["hR_min"].attrs["units"] = "m"

msh_grp.create_dataset(name = "hR_max",
                       shape = (1,calc_props.hR_max.size()),
                      #  chunks = True,
                       compression = "gzip",
                      #  shuffle = True,
                      #  fletcher32 = True,
                       dtype='float64',
                       data = calc_props.hR_max)

msh_grp["hR_max"].attrs["units"] = "m"


msh_grp.create_dataset(name = "vertices_ids",
                       shape = (1,dpp_vertices.size()),
                      #  chunks = True,
                       compression = "gzip",
                      #  shuffle = True,
                      #  fletcher32 = True,
                       dtype='int64',
                       data = dpp_vertices)

msh_grp.create_dataset(name = "edges_from_verticesids",
                       shape = ((dpp_edges.size()/2),2),
                      #  chunks = True,
                       compression = "gzip",
                      #  shuffle = True,
                      #  fletcher32 = True,
                       dtype='int64',
                       data = dpp_edges)

msh_grp.create_dataset(name = "polygons_from_vertices_ids",
                       shape = (dpp_num_polygons,dpp_num_of_polygons_max_vertices),
                      #  chunks = True,
                       compression = "gzip",
                      #  shuffle = True,
                      #  fletcher32 = True,
                       dtype='int64',
                       data = dpp_polygons_vids)

msh_grp.create_dataset(name = "polygons_from_edges_ids",
                       shape = (dpp_num_polygons,dpp_num_of_polygons_max_vertices),
                      #  chunks = True,
                       compression = "gzip",
                      #  shuffle = True,
                      #  fletcher32 = True,
                       dtype='int64',
                       data = dpp_polygons_eids)

# boundary conditions data
bc_grp = h5f.create_group("boundary_conditions")
bcid_type = numpy.dtype([('bc_id', numpy.int64), ('bc_value',numpy.float64)])
bcid_vec = numpy.empty( vector_pair_bcid_bcval.size(), dtype = bcid_type )
i=0
for p in vector_pair_bcid_bcval:
  bcid_vec[i] = (p[0],p[1])
  i = i+1

bc_grp.create_dataset(name = "bcid_bcval",
                      shape = (1,vector_pair_bcid_bcval.size()),
                      # chunks = True,
                      compression = "gzip",
                      # shuffle = True,
                      # fletcher32 = True,
                      dtype = bcid_type,
                      data = bcid_vec)

# HHO data   
hho_grp = h5f.create_group("hho_data")                

hho_grp.create_dataset(name = "hho_face_degree",
                       shape = (1,1),
                      #  fletcher32 = True,
                       dtype='int64',
                       data = hho_face_degree)
                 
hho_grp.create_dataset(name = "hho_cell_degree",
                       shape = (1,1),
                      #  fletcher32 = True,
                       dtype='int64',
                       data = hho_cell_degree)
                 
hho_grp.create_dataset(name = "hho_grad_degree",
                       shape = (1,1),
                      #  fletcher32 = True,
                       dtype='int64',
                       data = hho_grad_degree)

# linear system data (sparse matrix and othe things obtained after a call to solve)

ls_grp = h5f.create_group("linear_system")

# transform the sparse matrix into something we can export with hdf5
my_np_type = numpy.dtype([('r', numpy.int64), ('c', numpy.int64), ('v',numpy.float64)])
arr = numpy.empty( sp_mat.size(), dtype = my_np_type )
i=0
for a in sp_mat:
  arr[i] = (a.r,a.c,a.v)
  i = i+1

ls_grp.create_dataset(name = "sparse_matrix",
                      shape = (1,sp_mat.size()),
                      # chunks = True,
                      compression = "gzip",
                      # shuffle = True,
                      # fletcher32 = True,
                      dtype = my_np_type,
                      data = arr)

ls_grp.create_dataset(name = "Tp_per_edge",
                      shape = (1,calc_props.Tp_per_edge.size()),
                      # chunks = True,
                      compression = "gzip",
                      # shuffle = True,
                      # fletcher32 = True,
                      dtype='float64',
                      data = calc_props.Tp_per_edge)

ls_grp["Tp_per_edge"].attrs["units"] = "m"

ls_grp.create_dataset(name = "residual",
                      shape = (1,1),
                      # fletcher32 = True,
                      dtype='float64',
                      data = residual)

# properties calculated after the system has been solved

props_grp = h5f.create_group("calculated_props")

props_grp.create_dataset(name = "flux_per_edge",
                         shape = (1,calc_props.flux_per_edge.size()),
                        #  chunks = True,
                         compression = "gzip",
                        #  shuffle = True,
                        #  fletcher32 = True,
                         dtype='float64',
                         data = calc_props.flux_per_edge)

props_grp["flux_per_edge"].attrs["units"] = "m3 s-1"

props_grp.create_dataset(name = "press_per_cell",
                         shape = (1,calc_props.press_per_cell.size()),
                        #  chunks = True,
                         compression = "gzip",
                        #  shuffle = True,
                        #  fletcher32 = True,
                         dtype='float64',
                         data = calc_props.press_per_cell)

props_grp["press_per_cell"].attrs["units"] = "m"

props_grp.create_dataset(name = "Q1",
                         shape = (1,1),
                        #  fletcher32 = True,
                         dtype='float64',
                         data = calc_props.Q1)

props_grp["Q1"].attrs["units"] = "m3 s-1"

props_grp.create_dataset(name = "Q2",
                         shape = (1,1),
                        #  fletcher32 = True,
                         dtype='float64',
                         data = calc_props.Q2)

props_grp["Q2"].attrs["units"] = "m3 s-1"

props_grp.create_dataset(name = "equivalent_permeability",
                         shape = (1,1),
                        #  fletcher32 = True,
                         dtype='float64',
                         data = equ_perm)

props_grp["equivalent_permeability"].attrs["units"] = "m2 s-1"

print("I/O to HDF5 was a success, now closing file and exiting...")

# close properly the output file
h5f.close()

print("Maximum amount of RAM memory used is : %f MiB" % NEFpp.getPeakRSS_MiB())
